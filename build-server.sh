#!/bin/bash

set -a
. ./Config.txt
set +a

mkdir -p $ARCHIVED_SERVER_DIR

echo "--- COOK PROJECT SERVER ---"
$UE4_ROOT/Engine/Build/BatchFiles/RunUAT.sh \
    BuildCookRun -project="$GAME_UPROJECT" \
    -debug -VeryVerbose -nop4 -build -cook -compressed -stage \
    -noclient -server -serverplatform=Linux -serverconfig=$PACKAGE_CONFIG -LinuxNoEditor \
    -pak -archive -archivedirectory="$ARCHIVED_SERVER_DIR" \
    -utf8output
