!#/bin/bash

set -a
. ./Config.txt
set +a

mkdir -p $ARCHIVED_CLIENT_DIR

echo "--- COOK PROJECT CLIENT ---"
$UE4_ROOT/Engine/Build/BatchFiles/RunUAT.sh \
    BuildCookRun -project="$GAME_UPROJECT" \
    -nop4 -build -cook -compressed -stage \
    -platform=Linux -clientconfig=$PACKAGE_CONFIG \
    -pak -archive -archivedirectory="$ARCHIVED_CLIENT_DIR" \
    -utf8output
