// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "GameplayTagContainer.h"
#include "InGamePlayerState.generated.h"

class AUnit;
class ABuilding;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerState_Unit, AInGamePlayerState*, PlayerState, AUnit*, Unit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerState_Building, AInGamePlayerState*, PlayerState, ABuilding*, Building);

/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGamePlayerState : public APlayerState
{
	GENERATED_BODY()

protected:
	UPROPERTY()
		TArray<AUnit*> Units;

	UPROPERTY()
		TArray<ABuilding*> Buildings;

public:
	UFUNCTION(BlueprintPure)
		TArray<AUnit*>& GetUnits() { return Units; }

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*>& GetBuildings() { return Buildings; }

	UFUNCTION(BlueprintPure)
		TArray<AUnit*> GetAllUnitsOfType(FGameplayTag UnitType);

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*> GetAllBuildingsOfType(FGameplayTag BuildingType);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void AddUnitRPCM(AUnit* Unit);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void AddBuildingRPCM(ABuilding* Building);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void RemoveUnitRPCM(AUnit* Unit);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
		void RemoveBuildingRPCM(ABuilding* Building);

	UFUNCTION(BlueprintCallable)
		void AddUnit(AUnit* Unit);

	UFUNCTION(BlueprintCallable)
		void AddBuilding(ABuilding* Building);

	UFUNCTION(BlueprintCallable)
		void RemoveUnit(AUnit* Unit);

	UFUNCTION(BlueprintCallable)
		void RemoveBuilding(ABuilding* Building);

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Unit OnUnitAdded;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Unit OnUnitRemoved;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Building OnBuildingAdded;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerState_Building OnBuildingRemoved;
};
