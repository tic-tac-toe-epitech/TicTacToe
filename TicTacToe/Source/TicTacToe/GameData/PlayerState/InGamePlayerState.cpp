// Fill out your copyright notice in the Description page of Project Settings.


#include "InGamePlayerState.h"
#include "InGame/Unit/Unit.h"
#include "InGame//Building/Building.h"
#include "TicTacToe.h"

TArray<AUnit*> AInGamePlayerState::GetAllUnitsOfType(FGameplayTag UnitType)
{
	return AUnit::GetUnitsOfType(GetUnits(), UnitType);
}

TArray<ABuilding*> AInGamePlayerState::GetAllBuildingsOfType(FGameplayTag BuildingType)
{
	return ABuilding::GetBuildingsOfType(GetBuildings(), BuildingType);
}

void AInGamePlayerState::AddUnitRPCM_Implementation(AUnit* Unit)
{
	if (Unit)
	{
		AddUnit(Unit);
	}
}

void AInGamePlayerState::AddBuildingRPCM_Implementation(ABuilding* Building)
{
	if (Building)
	{
		AddBuilding(Building);
	}
}

void AInGamePlayerState::RemoveUnitRPCM_Implementation(AUnit* Unit)
{
	if (Unit)
	{
		RemoveUnit(Unit);
	}
}

void AInGamePlayerState::RemoveBuildingRPCM_Implementation(ABuilding* Building)
{
	if (Building)
	{
		RemoveBuilding(Building);
	}
}

void AInGamePlayerState::AddUnit(AUnit* Unit)
{
	if (Unit && !Units.Contains(Unit))
	{
		AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(Unit->GetOwner());
		if (ownerAsInGamePlayerState && ownerAsInGamePlayerState != this)
		{
			ownerAsInGamePlayerState->RemoveUnit(Unit);
		}
		if (HasAuthority())
		{
			if (Unit->GetOwner() != this)
			{
				Unit->SetOwner(this);
			}
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("SERVER: AInGamePlayerState::AddUnit: %s correctly added unit %s."),
				*GetNameSafe(this),
				*GetNameSafe(Unit));
		}
		else
		{
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("CLIENT: AInGamePlayerState::AddUnit: %s correctly added unit %s."),
				*GetNameSafe(this),
				*GetNameSafe(Unit));
		}
		Units.AddUnique(Unit);
		OnUnitAdded.Broadcast(this, Unit);
	}
	else
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Log,
			TEXT("AInGamePlayerState::AddUnit : Unit already possessed by the player."))
	}
}

void AInGamePlayerState::RemoveUnit(AUnit* Unit)
{
	if (Unit && Units.Contains(Unit))
	{
		if (HasAuthority())
		{
			if (Unit->GetOwner() == this)
			{
				Unit->SetOwner(nullptr);
			}
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("SERVER: AInGamePlayerState::RemoveUnit: %s correctly removed unit %s."),
				*GetNameSafe(this),
				*GetNameSafe(Unit));
		}
		else
		{
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("CLIENT: AInGamePlayerState::RemoveUnit: %s correctly removed unit %s."),
				*GetNameSafe(this),
				*GetNameSafe(Unit));
		}
		Units.Remove(Unit);
		OnUnitRemoved.Broadcast(this, Unit);
	}
	else
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Log,
			TEXT("AInGamePlayerState::RemoveUnit : Unit not possessed by the player."))
	}

}

void AInGamePlayerState::AddBuilding(ABuilding* Building)
{
	if (Building && !Buildings.Contains(Building))
	{
		AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(Building->GetOwner());
		if (ownerAsInGamePlayerState && ownerAsInGamePlayerState != this)
		{
			ownerAsInGamePlayerState->RemoveBuilding(Building);
		}
		if (HasAuthority())
		{
			if (Building->GetOwner() != this)
			{
				Building->SetOwner(this);
			}
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("SERVER: AInGamePlayerState::AddBuilding: %s correctly added building %s."),
				*GetNameSafe(this),
				*GetNameSafe(Building));
		}
		else
		{
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("CLIENT: AInGamePlayerState::AddBuilding: %s correctly added building %s."),
				*GetNameSafe(this),
				*GetNameSafe(Building));
		}
		Buildings.AddUnique(Building);
		OnBuildingAdded.Broadcast(this, Building);
	}
	else
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Log,
			TEXT("AInGamePlayerState::AddBuilding : Building already possessed by the player."))
	}

}

void AInGamePlayerState::RemoveBuilding(ABuilding* Building)
{
	if (Building && Buildings.Contains(Building))
	{
		if (HasAuthority())
		{
			if (Building->GetOwner() == this)
			{
				Building->SetOwner(nullptr);
			}
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("SERVER: AInGamePlayerState::RemoveBuilding: %s correctly removed building %s."),
				*GetNameSafe(this),
				*GetNameSafe(Building));
		}
		else
		{
			UE_LOG(TicTacLogPlayerCivilization,
				Log,
				TEXT("CLIENT: AInGamePlayerState::RemoveBuilding: %s correctly removed building %s."),
				*GetNameSafe(this),
				*GetNameSafe(Building));
		}
		Buildings.Remove(Building);
		OnBuildingRemoved.Broadcast(this, Building);
	}
	else
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Log,
			TEXT("AInGamePlayerState::RemoveBuilding : Building not possessed by the player."))
	}
}
