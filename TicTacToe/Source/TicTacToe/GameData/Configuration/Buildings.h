#pragma once

#include "TicTacToe/GameData/Configuration/Building/BuildingConfig.h"
#include "Buildings.generated.h"

//!
//! @struct FBuildingsStruct
//! @brief Game buildings configuration.
//!
USTRUCT(BlueprintType)
struct FBuildingsStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Buildings constructor.
		//!
		FBuildingsStruct() : _Id("Building"),
							_Heart("Building.Heart", "Heart", 2500, FPriceStruct(0, 0, 0), 0, 0, false, 0, 0, FPriceStruct(10, 5, 2)),
							_CityHall("Building.CityHall", "City Hall", 2500, FPriceStruct(0, 0, 0), 0, 0, false, 0, 0, FPriceStruct(200, 150, 100)),
							_Laboratory("Building.Laboratory", "Laboratory", 500, FPriceStruct(200, 150, 50), 0, 0, false, 30, 0, FPriceStruct(0, 0, 0)),
							_Sawmill("Building.Sawmill", "Sawmill", 500, FPriceStruct(200, 0, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Quarry("Building.Quarry", "Quarry", 500, FPriceStruct(500, 0, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Outpost("Building.Outpost", "Outpost", 2500, FPriceStruct(1000, 300, 100), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Forge("Building.Forge", "Forge", 500, FPriceStruct(0, 200, 100), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Hospital("Building.Hospital", "Hospital", 500, FPriceStruct(400, 200, 100), 10, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Machinery("Building.Machinery", "Machinery", 500, FPriceStruct(500, 300, 100), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Barracks("Building.Barracks", "Barracks", 500, FPriceStruct(300, 50, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_ShootingCamp("Building.ShootingCamp", "ShootingCamp", 500, FPriceStruct(300, 100, 0), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_SpacePort("Building.SpacePort", "SpacePort", 500, FPriceStruct(0, 400, 200), 0, 0, true, 30, 60, FPriceStruct(0, 0, 0)),
							_Tower("Building.Tower", "Tower", 1000, FPriceStruct(0, 100, 20), 0, 5, false, 30, 60, FPriceStruct(0, 0, 0)),
							_Wall("Building.Wall", "Wall", 2000, FPriceStruct(0, 20, 0), 0, 0, true, 30, 0, FPriceStruct(0, 0, 0)) { }

	public:
		// Building ID.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FString				_Id;
		// Heart building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Heart;
		// City Hall building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_CityHall;
		// Laboratory building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Laboratory;
		// Sawmill building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Sawmill;
		// Quarry building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Quarry;
		// Outpost building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Outpost;
		// Forge building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Forge;
		// Hospital building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Hospital;
		// Machinery building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Machinery;
		// Barracks building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Barracks;
		// Shooting Camp building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_ShootingCamp;
		// Space Port building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_SpacePort;
		// Tower building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Tower;
		// Wall building.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FBuildingStruct		_Wall;
};