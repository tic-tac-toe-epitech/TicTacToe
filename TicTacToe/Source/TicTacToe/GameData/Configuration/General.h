#pragma once

#include "TicTacToe/GameData/Configuration/General/Network.h"
#include "General.generated.h"

//!
//! @struct FGeneralStruct
//! @brief General configuration for TicTacToe server.
//!
USTRUCT(BlueprintType)
struct FGeneralStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default constructor for general configuration sturcture. Default values may not be valid.
		//!
		FGeneralStruct() : _Network("localhost", 4242) {}

	public:
		// Network configuration.
		UPROPERTY(BlueprintReadOnly, Category = "General")
		FNetworkStruct	_Network;
};