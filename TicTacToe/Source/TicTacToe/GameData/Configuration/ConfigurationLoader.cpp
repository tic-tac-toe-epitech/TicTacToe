// Fill out your copyright notice in the Description page of Project Settings.

#include "ConfigurationLoader.h"

FBuildingsStruct	UConfigurationLoader::LoadAndGetBuildingsConfig()
{
	LoadBuildingsConfig();
	return (_Buildings);
}

void				UConfigurationLoader::LoadBuildingsConfig()
{
	TSharedPtr<FJsonObject>	jsonObject;

	LoadJsonObject("buildings.json", jsonObject);
	if (jsonObject.Get() != NULL && jsonObject.IsValid())
		ExtractBuildingsStructFromJsonInstance(jsonObject);
}

FMapStruct	UConfigurationLoader::LoadAndGetMapConfig()
{
	LoadMapConfig();
	return (_Map);
}

void		UConfigurationLoader::LoadMapConfig()
{
	TSharedPtr<FJsonObject>	jsonObject;
	
	LoadJsonObject("map.json", jsonObject);
	if (jsonObject.Get() != NULL && jsonObject.IsValid())
		ExtractMapStructFromJsonInstance(jsonObject);
}

FResourcesStruct	UConfigurationLoader::LoadAndGetResourcesConfig()
{
	LoadResourcesConfig();
	return (_Resources);
}

void				UConfigurationLoader::LoadResourcesConfig()
{
	TSharedPtr<FJsonObject>	jsonObject;
	
	LoadJsonObject("resources.json", jsonObject);
	if (jsonObject.Get() != NULL && jsonObject.IsValid())
		ExtractResourcesStructFromJsonInstance(jsonObject);
}

void			UConfigurationLoader::LoadGeneralConfig()
{
	TSharedPtr<FJsonObject>	jsonObject;

	LoadJsonObject("general.json", jsonObject);
	if (jsonObject.Get() != NULL && jsonObject.IsValid())
		ExtractGeneralStructFromJsonInstance(jsonObject);
}

FGeneralStruct	UConfigurationLoader::LoadAndGetGeneralConfig()
{
	LoadGeneralConfig();
	return (_General);
}

void			UConfigurationLoader::LoadUnitsConfig()
{
	TSharedPtr<FJsonObject>	jsonObject;

	LoadJsonObject("units.json", jsonObject);
	if (jsonObject.Get() != NULL && jsonObject.IsValid())
		ExtractUnitsStructFromJsonInstance(jsonObject);
}

FUnitsStruct	UConfigurationLoader::LoadAndGetUnitsConfig()
{
	LoadUnitsConfig();
	return (_Units);
}

const TSharedPtr<FJsonObject>	UConfigurationLoader::LoadJsonObject(const FString &fileName, TSharedPtr<FJsonObject> &jsonObject)
{
	UE_LOG(Configuration, Log, TEXT("Game configuration is load from the next directory : %s."), *(DefaultConfigurationDirectoryPath));

	FString							configurationFilePath = DefaultConfigurationDirectoryPath + '/' + fileName;
	FString							configurationFileContent;

	FFileHelper::LoadFileToString(configurationFileContent, *(configurationFilePath));

	TSharedRef<TJsonReader<TCHAR>>	jsonReader = TJsonReaderFactory<TCHAR>::Create(configurationFileContent);

	jsonObject = MakeShareable(new FJsonObject);
	if (!FJsonSerializer::Deserialize(jsonReader, jsonObject))
	{
		UE_LOG(Configuration, Error, TEXT("Error while deserialize the next configuration json file %s."), *(configurationFilePath));
		return (TSharedPtr<FJsonObject>(NULL));
	}
	UE_LOG(Configuration, Log, TEXT("Configuration json file %s has been deserialize."), *(configurationFilePath));
	if (!jsonObject.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("Configuration json file %s is not valid."), *(configurationFilePath));
		return (TSharedPtr<FJsonObject>(NULL));
	}
	UE_LOG(Configuration, Log, TEXT("Configuration json file %s is valid."), *(configurationFilePath));
	return (jsonObject);
}

void				UConfigurationLoader::ExtractBuildingsStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject)
{
	const TSharedPtr<FJsonObject>	jsonObjectUnits = jsonObject->GetObjectField(TEXT("building"));

	if (!jsonObjectUnits.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("Buildings field not valid."));
		return;
	}
	_Buildings._Id = jsonObjectUnits->GetStringField(TEXT("id"));
	UE_LOG(Configuration, Log, TEXT("Buildings.id: %s."), *(_Buildings._Id));
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("heart")), _Buildings._Heart);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("city_hall")), _Buildings._CityHall);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("laboratory")), _Buildings._Laboratory);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("sawmill")), _Buildings._Sawmill);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("quarry")), _Buildings._Quarry);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("outpost")), _Buildings._Outpost);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("forge")), _Buildings._Forge);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("hospital")), _Buildings._Hospital);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("machinery")), _Buildings._Machinery);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("barracks")), _Buildings._Barracks);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("shooting_camp")), _Buildings._ShootingCamp);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("space_port")), _Buildings._SpacePort);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("tower")), _Buildings._Tower);
	ExtractSpecificBuildingStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("wall")), _Buildings._Wall);
}

void				UConfigurationLoader::ExtractMapStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject)
{
	const TSharedPtr<FJsonObject>	jsonObjectMap = jsonObject->GetObjectField(TEXT("map"));
	const TSharedPtr<FJsonObject>	jsonObjectMapVictoryTerms = jsonObjectMap->GetObjectField(TEXT("victory_terms"));

	if (!jsonObjectMap.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("Map field not valid."));
		return;
	}
	_Map._Id = jsonObjectMap->GetStringField(TEXT("id"));
	_Map._DefaultMaxPopulationByPlayer = jsonObjectMap->GetIntegerField(TEXT("default_max_population_by_player"));
	UE_LOG(Configuration, Log, TEXT("Map.id: %s."), *(_Map._Id));
	UE_LOG(Configuration, Log, TEXT("Map.default_max_population_by_player: %d."), _Map._DefaultMaxPopulationByPlayer);
	if (!jsonObjectMapVictoryTerms.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("Map.VictoryTerms field not valid."));
		return;
	}
	_Map._VictoryTerms._LineGoal = jsonObjectMapVictoryTerms->GetIntegerField(TEXT("line_goal"));
	_Map._VictoryTerms._BAllowVertical = jsonObjectMapVictoryTerms->GetBoolField(TEXT("allow_vertical"));
	_Map._VictoryTerms._BAllowHorizontal = jsonObjectMapVictoryTerms->GetBoolField(TEXT("allow_horizontal"));
	_Map._VictoryTerms._BAllowDiagonals = jsonObjectMapVictoryTerms->GetBoolField(TEXT("allow_diagonals"));
	UE_LOG(Configuration, Log, TEXT("Map.victory_terms.line_goal: %d."), _Map._VictoryTerms._LineGoal);
	UE_LOG(Configuration, Log, TEXT("Map.victory_terms.allow_vertical: %d."), _Map._VictoryTerms._BAllowVertical);
	UE_LOG(Configuration, Log, TEXT("Map.victory_terms.allow_horizontal: %d."), _Map._VictoryTerms._BAllowHorizontal);
	UE_LOG(Configuration, Log, TEXT("Map.victory_terms.allow_diagonals: %d."), _Map._VictoryTerms._BAllowDiagonals);
}

void	UConfigurationLoader::ExtractResourcesStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject)
{
	const TSharedPtr<FJsonObject>	jsonObjectResources = jsonObject->GetObjectField(TEXT("resources"));
	const TSharedPtr<FJsonObject>	jsonObjectResourcesSteel = jsonObjectResources->GetObjectField(TEXT("steel"));
	const TSharedPtr<FJsonObject>	jsonObjectResourcesGold = jsonObjectResources->GetObjectField(TEXT("gold"));
	const TSharedPtr<FJsonObject>	jsonObjectResourcesWood = jsonObjectResources->GetObjectField(TEXT("wood"));

	if (!jsonObjectResources.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("'resources' field not valid."));
		return;
	}
	_Resources._Id = jsonObjectResources->GetStringField(TEXT("id"));
	UE_LOG(Configuration, Log, TEXT("Resources.id: %s."), *(_Resources._Id));
	if (!jsonObjectResourcesSteel.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("'resources.steel' field not valid."));
		return;
	}
	_Resources._Steel._Id = jsonObjectResourcesSteel->GetStringField(TEXT("id"));
	_Resources._Steel._Name = jsonObjectResourcesSteel->GetStringField(TEXT("name"));
	UE_LOG(Configuration, Log, TEXT("resources.steel.id: %s."), *(_Resources._Steel._Id));
	UE_LOG(Configuration, Log, TEXT("resources.steel.name: %s."), *(_Resources._Steel._Name));
	if (!jsonObjectResourcesGold.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("'resources.gold' field not valid."));
		return;
	}
	_Resources._Gold._Id = jsonObjectResourcesGold->GetStringField(TEXT("id"));
	_Resources._Gold._Name = jsonObjectResourcesGold->GetStringField(TEXT("name"));
	UE_LOG(Configuration, Log, TEXT("resources.gold.id: %s."), *(_Resources._Gold._Id));
	UE_LOG(Configuration, Log, TEXT("resources.gold.name: %s."), *(_Resources._Gold._Name));
	if (!jsonObjectResourcesWood.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("'resources.wood' field not valid."));
		return;
	}
	_Resources._Wood._Id = jsonObjectResourcesWood->GetStringField(TEXT("id"));
	_Resources._Wood._Name = jsonObjectResourcesWood->GetStringField(TEXT("name"));
	UE_LOG(Configuration, Log, TEXT("resources.wood.id: %s."), *(_Resources._Wood._Id));
	UE_LOG(Configuration, Log, TEXT("resources.wood.name: %s."), *(_Resources._Wood._Name));
}

void	UConfigurationLoader::ExtractGeneralStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject)
{
	const TSharedPtr<FJsonObject>	jsonObjectGeneral = jsonObject->GetObjectField(TEXT("general"));
	const TSharedPtr<FJsonObject>	jsonObjectGeneralNetwork = jsonObjectGeneral->GetObjectField(TEXT("network"));

	if (!jsonObjectGeneral.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("'general' field not valid."));
		return;
	}
	if (!jsonObjectGeneralNetwork.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("'general.network' field not valid."));
		return;
	}
	_General._Network._Url = jsonObjectGeneralNetwork->GetStringField(TEXT("url"));
	UE_LOG(Configuration, Log, TEXT("general.network.url: %s."), *(_General._Network._Url));
	_General._Network._Port = jsonObjectGeneralNetwork->GetIntegerField(TEXT("port"));
	UE_LOG(Configuration, Log, TEXT("general.network.port: %d."), _General._Network._Port);
}

void	UConfigurationLoader::ExtractUnitsStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject)
{
	const TSharedPtr<FJsonObject>	jsonObjectUnits = jsonObject->GetObjectField(TEXT("units"));

	if (!jsonObjectUnits.IsValid())
	{
		UE_LOG(Configuration, Error, TEXT("Units field not valid."));
		return;
	}
	_Units._Id = jsonObjectUnits->GetStringField(TEXT("id"));
	UE_LOG(Configuration, Log, TEXT("Units.id: %s."), *(_Units._Id));
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("soldier")), _Units._Soldier);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("shooter")), _Units._Shooter);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("flywheel_motor")), _Units._FlywheelMotor);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("artillery")), _Units._Artillery);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("villagers")), _Units._Villagers);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("cyborg")), _Units._Cyborg);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("flame_thrower")), _Units._FlameThrower);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("ship")), _Units._Ship);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("medical_engineer")), _Units._MedicalEngineer);
	ExtractSpecificUnitStructFromJsonInstance(jsonObjectUnits->GetObjectField(TEXT("scout")), _Units._Scout);
}

void	UConfigurationLoader::ExtractSpecificUnitStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject, FUnitStruct &unit)
{
	const TSharedPtr<FJsonObject>			jsonObjectDamage = jsonObject->GetObjectField(TEXT("damage"));
	const TArray<TSharedPtr<FJsonValue>>	*objArray;
	const TSharedPtr<FJsonObject>			jsonObjectPrice = jsonObject->GetObjectField(TEXT("price"));

	unit._Id = jsonObject->GetStringField(TEXT("id"));
	UE_LOG(Configuration, Log, TEXT("Units.unit.id: %s."), *(unit._Id));
	unit._Name = jsonObject->GetStringField(TEXT("name"));
	UE_LOG(Configuration, Log, TEXT("%s.name: %s."), *(unit._Id), *(unit._Name));
	unit._Hp = jsonObject->GetIntegerField(TEXT("hp"));
	UE_LOG(Configuration, Log, TEXT("%s.hp: %d."), *(unit._Id), unit._Hp);
	unit._Scope = jsonObject->GetIntegerField(TEXT("scope"));
	UE_LOG(Configuration, Log, TEXT("%s.scope: %d."), *(unit._Id), unit._Scope);
	unit._Damage._Value = jsonObjectDamage->GetIntegerField(TEXT("value"));
	UE_LOG(Configuration, Log, TEXT("%s.damage.value: %d."), *(unit._Id), unit._Damage._Value);
	if (jsonObjectDamage->TryGetArrayField(FString("modifier"), objArray))
	{
		for (int32 i = 0; i < objArray->Num(); ++i)
		{
			TSharedPtr<FJsonValue>		value = (*objArray)[i];;
			TSharedPtr<FJsonObject>		json = value->AsObject();
			FUnitDamageModifierStruct	modifierDamage;

			modifierDamage._Id = json->GetStringField(TEXT("id"));
			UE_LOG(Configuration, Log, TEXT("%s.damage.modifier.%d.id: %s."), *(unit._Id), i, *(modifierDamage._Id));
			modifierDamage._Value = json->GetNumberField(TEXT("value"));
			UE_LOG(Configuration, Log, TEXT("%s.damage.modifier.%d.value: %f."), *(unit._Id), i, modifierDamage._Value);
			unit._Damage._Modifiers.Add(modifierDamage);
		}
	}
	unit._Speed = jsonObject->GetNumberField(TEXT("speed"));
	UE_LOG(Configuration, Log, TEXT("%s.speed: %f."), *(unit._Id), unit._Speed);
	unit._Price._Wood = jsonObjectPrice->GetIntegerField(TEXT("wood"));
	UE_LOG(Configuration, Log, TEXT("%s.price.wood: %d."), *(unit._Id), unit._Price._Wood);
	unit._Price._Steel = jsonObjectPrice->GetIntegerField(TEXT("steel"));
	UE_LOG(Configuration, Log, TEXT("%s.price.steel: %d."), *(unit._Id), unit._Price._Steel);
	unit._Price._Gold = jsonObjectPrice->GetIntegerField(TEXT("gold"));
	UE_LOG(Configuration, Log, TEXT("%s.price.gold: %d."), *(unit._Id), unit._Price._Gold);
	unit._HiringTime = jsonObject->GetIntegerField(TEXT("hiring_time"));
	UE_LOG(Configuration, Log, TEXT("%s.hiring_time: %d."), *(unit._Id), unit._HiringTime);
	unit._ResearchTime = jsonObject->GetIntegerField(TEXT("research_time"));
	UE_LOG(Configuration, Log, TEXT("%s.research_time: %f."), *(unit._Id), unit._ResearchTime);
}

void	UConfigurationLoader::ExtractSpecificBuildingStructFromJsonInstance(const TSharedPtr<FJsonObject> &jsonObject, FBuildingStruct &building)
{
	const TSharedPtr<FJsonObject>			jsonObjectPrice = jsonObject->GetObjectField(TEXT("price"));
	const TSharedPtr<FJsonObject>			jsonObjectUpgradeCost = jsonObject->GetObjectField(TEXT("upgrade_cost"));

	building._Id = jsonObject->GetStringField(TEXT("id"));
	UE_LOG(Configuration, Log, TEXT("Buildings.Building.id: %s."), *(building._Id));
	building._Name = jsonObject->GetStringField(TEXT("name"));
	UE_LOG(Configuration, Log, TEXT("%s.name: %s."), *(building._Id), *(building._Name));
	building._Hp = jsonObject->GetIntegerField(TEXT("hp"));
	UE_LOG(Configuration, Log, TEXT("%s.hp: %d."), *(building._Id), building._Hp);
	building._Price._Wood = jsonObjectPrice->GetIntegerField(TEXT("wood"));
	UE_LOG(Configuration, Log, TEXT("%s.price.wood: %d."), *(building._Id), building._Price._Wood);
	building._Price._Steel = jsonObjectPrice->GetIntegerField(TEXT("steel"));
	UE_LOG(Configuration, Log, TEXT("%s.price.steel: %d."), *(building._Id), building._Price._Steel);
	building._Price._Gold = jsonObjectPrice->GetIntegerField(TEXT("gold"));
	UE_LOG(Configuration, Log, TEXT("%s.price.gold: %d."), *(building._Id), building._Price._Gold);
	building._Heal = jsonObject->GetIntegerField(TEXT("heal"));
	UE_LOG(Configuration, Log, TEXT("%s.heal: %d."), *(building._Id), building._Heal);
	building._BuildAllow = jsonObject->GetBoolField(TEXT("build_allow"));
	UE_LOG(Configuration, Log, TEXT("%s.build_allow: %d."), *(building._Id), building._BuildAllow);
	building._BuildTime = jsonObject->GetIntegerField(TEXT("build_time"));
	UE_LOG(Configuration, Log, TEXT("%s.build_time: %d."), *(building._Id), building._BuildTime);
	building._ResearchTime = jsonObject->GetIntegerField(TEXT("research_time"));
	UE_LOG(Configuration, Log, TEXT("%s.research_time: %d."), *(building._Id), building._ResearchTime);
	building._UpgradeCost._Wood = jsonObjectUpgradeCost->GetIntegerField(TEXT("wood"));
	UE_LOG(Configuration, Log, TEXT("%s.upgrade_cost.wood: %d."), *(building._Id), building._UpgradeCost._Wood);
	building._UpgradeCost._Steel = jsonObjectUpgradeCost->GetIntegerField(TEXT("steel"));
	UE_LOG(Configuration, Log, TEXT("%s.upgrade_cost.steel: %d."), *(building._Id), building._UpgradeCost._Steel);
	building._UpgradeCost._Gold = jsonObjectUpgradeCost->GetIntegerField(TEXT("gold"));
	UE_LOG(Configuration, Log, TEXT("%s.upgrade_cost.gold: %d."), *(building._Id), building._UpgradeCost._Gold);
}