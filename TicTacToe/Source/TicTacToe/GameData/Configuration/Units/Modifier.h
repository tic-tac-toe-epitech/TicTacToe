#pragma once

#include "CoreMinimal.h"
#include "Modifier.generated.h"

//!
//! @struct FUnitDamageModifierStruct
//! @brief Unit damage modifier (contain one unit damage modifier information's).
//!
USTRUCT(BlueprintType)
struct FUnitDamageModifierStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default damage modifier structure constructor. Default value consider has no modifier.
		//!
		FUnitDamageModifierStruct() : _Id(""), _Value(0) {}
		//!
		//! @brief Damage modifier constructor.
		//! @param id Damage ID, target where damage are upgrade or downgrade.
		//! @param value Damage multiplier.
		//!
		FUnitDamageModifierStruct(const FString &id, float value) : _Id(id), _Value(value) {}

	public:
		// Damage ID, target where damage are upgrade or downgrade.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit.Damage.Modifier")
		FString		_Id;
		// Damage multiplier.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit.Damage.Modifier")
		float		_Value;
};