#pragma once

#include "TicTacToe/GameData/Configuration/Units/Modifier.h"
#include "Damage.generated.h"

//!
//! @struct FUnitDamageStruct
//! @brief Unit damage (contain one unit damage information's).
//!
USTRUCT(BlueprintType)
struct FUnitDamageStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default damage structure constructor. Default value define the damage has none.
		//!
		FUnitDamageStruct() : _Value(0), _Modifiers() {}

	public:
		// Damage value.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit.Damage")
		int32								_Value;
		// Damage modifiers, damage multiplier can be apply on specific target.
		UPROPERTY(BlueprintReadOnly, Category = "Units.Unit.Damage")
		TArray<FUnitDamageModifierStruct>	_Modifiers;
};