#pragma once

#include "CoreMinimal.h"
#include "Network.generated.h"

//!
//! @struct FNetworkStruct
//! @brief General network configuration for TicTacToe server.
//!
USTRUCT(BlueprintType)
struct FNetworkStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default general network configuration structure constructor. All value set in this contructor are invalid.
		//!
		FNetworkStruct() : _Url(""), _Port(0) {}
		//!
		//! @brief Network (general) configuration constructor.
		//! @param url Server URL.
		//! @param port Server port.
		//!
		FNetworkStruct(const FString &url, int32 port) : _Url(url), _Port(port) {}

	public:
		// Server URL.
		UPROPERTY(BlueprintReadOnly, Category = "General.Network")
		FString	_Url;
		// Server port.
		UPROPERTY(BlueprintReadOnly, Category = "General.Network")
		int32	_Port;
};