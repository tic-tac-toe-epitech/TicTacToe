#pragma once

#include "TicTacToe/GameData/Configuration/Resources/Resource.h"
#include "Resources.generated.h"

//!
//! @struct FResourcesStruct
//! @brief Resources configuration.
//!
USTRUCT(BlueprintType)
struct FResourcesStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default constructor for resources configuration.
		//!
		FResourcesStruct() : _Id("Resources"), _Steel("Resources.Steel", "Steel"), _Gold("Resources.Gold", "Gold"), _Wood("Resources.Wood", "Wood") {}

	public:
		// Map ID.
		UPROPERTY(BlueprintReadOnly, Category = "Resources")
		FString			_Id;
		// Information about the steel resource.
		UPROPERTY(BlueprintReadOnly, Category = "Resources")
		FResourceStruct	_Steel;
		// Information about the gold resource.
		UPROPERTY(BlueprintReadOnly, Category = "Resources")
		FResourceStruct	_Gold;
		// Information about the wood resource.
		UPROPERTY(BlueprintReadOnly, Category = "Resources")
		FResourceStruct	_Wood;
};