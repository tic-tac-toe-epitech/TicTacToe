#pragma once

#include "CoreMinimal.h"
#include "TicTacToe/TicTacToe.h"
#include "Price.generated.h"

//!
//! @struct FUnitPriceStruct
//! @brief Unit price (contain one unit price information's).
//!
USTRUCT(BlueprintType)
struct FPriceStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		// Wood price.
		UPROPERTY(BlueprintReadOnly, Category = "Shared.Price")
		int32	_Wood;
		// Steel price.
		UPROPERTY(BlueprintReadOnly, Category = "Shared.Price")
		int32	_Steel;
		// Gold price.
		UPROPERTY(BlueprintReadOnly, Category = "Shared.Price")
		int32	_Gold;
	
	public:
		//!
		//! @brief Default unit price structure constructor. Default value design a free unit.
		//!
		FPriceStruct() : _Wood(0), _Steel(0), _Gold(0) {}
		//!
		//! @brief Unit price constructor.
		//! @param wood Wood price.
		//! @param steel Steel price.
		//! @param gold Gold price.
		//!
		FPriceStruct(int32 wood, int32 steel, int32 gold) : _Wood(wood), _Steel(steel), _Gold(gold) {}

	public:
		//!
		//! @brief Equal operator overload, allow to compare two price.
		//! @param price Input price to compare with the current.
		//! @return State, equal or not.
		//!
		FORCEINLINE  bool	operator==(const FPriceStruct &price) const { return (price._Wood == _Wood && price._Steel == _Steel && price._Gold == _Gold); }
		//!
		//! @brief Upper operator overload, allow to compare two price.
		//! @param price Input price to compare with the current.
		//! @return State, upper or not.
		//!
		FORCEINLINE bool	operator>(const FPriceStruct &price) const { return (price._Wood > _Wood && price._Steel > _Steel && price._Gold > _Gold); }
		//!
		//! @brief Smaller operator overload, allow to compare two price.
		//! @param price Input price to compare with the current.
		//! @return State, smaller or not.
		//!
		FORCEINLINE bool	operator<(const FPriceStruct &price) const { return (price._Wood < _Wood && price._Steel < _Steel && price._Gold < _Gold); }

	public:
		FORCEINLINE void	logIt() const { UE_LOG(Configuration, Error, TEXT("Wood : %d, Steel %d & Gold : %d"), _Wood, _Steel, _Gold); }
};