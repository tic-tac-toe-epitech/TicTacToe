#pragma once

#include "TicTacToe/GameData/Configuration/Units/UnitConfig.h"
#include "Units.generated.h"

//!
//! @struct FUnitsStruct
//! @brief Game units configuration.
//!
USTRUCT(BlueprintType)
struct FUnitsStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Units constructor.
		//!
		FUnitsStruct() : _Id("Units"),
						_Soldier("Units.Soldier", "Soldier", 100, 0, FUnitDamageStruct(), 1.0, FPriceStruct(100, 0, 0), 15, 0),
						_Shooter("Units.Shooter", "Shooter", 80, 3, FUnitDamageStruct(), 1.0, FPriceStruct(100, 15, 0), 15, 0),
						_FlywheelMotor("Units.FlywheelMotor", "Flywheel motor", 150, 2, FUnitDamageStruct(), 2.0, FPriceStruct(0, 100, 0), 15, 0),
						_Artillery("Units.Artillery", "Artillery", 150, 10, FUnitDamageStruct(), 0.5, FPriceStruct(0, 150, 50), 15, 0),
						_Villagers("Units.Villagers", "Villagers", 50, 0, FUnitDamageStruct(), 1.0, FPriceStruct(50, 0, 0), 15, 0),
						_Cyborg("Units.Cyborg", "Cyborg", 200, 0, FUnitDamageStruct(), 2.0, FPriceStruct(150, 30, 10), 15, 0),
						_FlameThrower("Units.FlameThrower", "Flame thrower", 150, 3, FUnitDamageStruct(), 1.0, FPriceStruct(150, 40, 15), 15, 60),
						_Ship("Units.Ship", "Ship", 250, 2, FUnitDamageStruct(), 3.0, FPriceStruct(0, 150, 75), 15, 60),
						_MedicalEngineer("Units.MedicalEngineer", "Medical Engineer", 150, 3, FUnitDamageStruct(), 2.0, FPriceStruct(150, 50, 25), 15, 0),
						_Scout("Units.Scout", "Scout", 100, 0, FUnitDamageStruct(), 4.0, FPriceStruct(50, 0, 0), 15, 60)
		{
			_Soldier._Damage._Value = 20;
			_Soldier._Damage._Modifiers.Add(FUnitDamageModifierStruct("Building", 5.0));
			_Shooter._Damage._Value = 25;
			_FlywheelMotor._Damage._Value = 25;
			_Artillery._Damage._Value = 30;
			_Villagers._Damage._Value = 5;
			_Cyborg._Damage._Value = 40;
			_FlameThrower._Damage._Value = 40;
			_Ship._Damage._Value = 50;
			_MedicalEngineer._Damage._Value = 15;
		}

	public:
		// Units ID.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FString				_Id;
		// Soldier unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Soldier;
		// Shooter unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Shooter;
		// Flywheel Motor unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_FlywheelMotor;
		// Artillery unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Artillery;
		// Villagers unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Villagers;
		// Cyborg unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Cyborg;
		// Flame thrower unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_FlameThrower;
		// Ship unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Ship;
		// Medical engineer unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_MedicalEngineer;
		// Scout unit.
		UPROPERTY(BlueprintReadOnly, Category = "Units")
		FUnitStruct	_Scout;
};