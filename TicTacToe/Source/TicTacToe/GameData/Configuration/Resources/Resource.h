#pragma once

#include "CoreMinimal.h"
#include "Resource.generated.h"

//!
//! @struct FResourceStruct
//! @brief Resource configuration (contain one resource information's).
//!
USTRUCT(BlueprintType)
struct FResourceStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default resource structure constructor. All value are invalid.
		//!
		FResourceStruct() : _Id(""), _Name("") {}
		//!
		//! @brief Resource configuration constructor.
		//! @param id Resource ID.
		//! @param name Resource name.
		//!
		FResourceStruct(const FString &id, const FString &name) : _Id(id), _Name(name) {}

	public:
		// Resource ID.
		UPROPERTY(BlueprintReadOnly, Category = "Resources.Resource")
		FString				_Id;
		// Resource name.
		UPROPERTY(BlueprintReadOnly, Category = "Resources.Resource")
		FString				_Name;
};