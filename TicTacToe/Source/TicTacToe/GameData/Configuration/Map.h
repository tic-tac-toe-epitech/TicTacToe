#pragma once

#include "TicTacToe/GameData/Configuration/Map/VictoryTerms.h"
#include "Map.generated.h"

//!
//! @struct Map
//! @brief Game map configuration.
//!
USTRUCT(BlueprintType)
struct FMapStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		//!
		//! @brief Default values for map configuration structure.
		//!
		FMapStruct() : _Id("Map"), _DefaultMaxPopulationByPlayer(20), _VictoryTerms() {}

	public:
		// Map ID.
		UPROPERTY(BlueprintReadOnly, Category = "Map")
		FString				_Id;
		// Default value for the population maximun allowed by player.
		UPROPERTY(BlueprintReadOnly, Category = "Map")
		int32				_DefaultMaxPopulationByPlayer;
		// Map victory terms for a game.
		UPROPERTY(BlueprintReadOnly, Category = "Map")
		FVictoryTermsStruct	_VictoryTerms;
};