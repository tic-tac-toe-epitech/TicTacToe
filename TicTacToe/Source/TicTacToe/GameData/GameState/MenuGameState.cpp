// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuGameState.h"

template <typename T>
FORCEINLINE T *ChangeCurrentScreen(UWorld *world, TSubclassOf<T> StartingWidgetClass)
{
	T *currentScreen = CreateWidget<T>(world, StartingWidgetClass);

	currentScreen->AddToViewport(9999);

	return currentScreen;
}

AMenuGameState::AMenuGameState() {
	static ConstructorHelpers::FClassFinder<UUserWidget> mainMenuWidget(TEXT("/Game/UI/UI_Menu"));
	static ConstructorHelpers::FClassFinder<UUserWidget> loadingScreen(TEXT("/Game/UI/Loading_Screen"));

	if (mainMenuWidget.Succeeded()) {
		this->MainMenuWidget = mainMenuWidget.Class;
	}

	if (loadingScreen.Succeeded()) {
		this->LoadingScreenWidget = loadingScreen.Class;
	}
}

void AMenuGameState::BeginPlay() {
	Super::BeginPlay();

	// Get general configuration settings
	this->ConfigurationLoader = NewObject<UConfigurationLoader>();
	this->GeneralConfig = this->ConfigurationLoader->LoadAndGetGeneralConfig();

	USearchGameWidget *currentScreen = ChangeCurrentScreen<USearchGameWidget>(GetWorld(), MainMenuWidget);

	FString mmUrlRaw = this->GeneralConfig._Network._Url;
	int32 mmPort = this->GeneralConfig._Network._Port;

	FIPv4Address mmUrl;

	if (!FIPv4Address::Parse(mmUrlRaw, mmUrl))
	{
		mmUrl = FIPv4Address(127, 0, 0, 1);
		mmPort = 8081;
	}

	// Instanciate matchmaking manager for the client side
	this->MMManager = NewObject<UMatchmakingManager>();
	this->MMManager->init(mmUrl, mmPort, std::bind(&AMenuGameState::readClientPacket, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

	currentScreen->SetStatus(USearchGameWidget::PROCESSING);

	// Connect to the matchmaking service
	if (this->MMManager->connect()) {
		currentScreen->SetStatus(USearchGameWidget::SUCCEED);
	}
	else {
		currentScreen->SetStatus(USearchGameWidget::FAILED);
	}

	currentScreen->Button_Play->OnClicked.AddDynamic(this, &AMenuGameState::searchGame);
}

void AMenuGameState::searchGame() {
	this->ClearViewport();
	ULoadingScreen *currentScreen = ChangeCurrentScreen<ULoadingScreen>(GetWorld(), LoadingScreenWidget);

	currentScreen->SetLoadingText("Waiting for player");

	this->MMManager->sendSearchMatch();
}

void AMenuGameState::readClientPacket(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json) {
	UE_LOG(LogTemp, Warning, TEXT("PACKET TYPE %d"), header.type);

	if (header.type == MessageUtils::JOIN_GAME_RESPONSE) {

		AsyncTask(ENamedThreads::GameThread, [this, json]() {
			UE_LOG(LogTemp, Warning, TEXT("CONNECTION TO %s"), *json->GetStringField("host"));
		
			ULoadingScreen *currentScreen = ChangeCurrentScreen<ULoadingScreen>(this->GetWorld(), LoadingScreenWidget);

			currentScreen->SetLoadingText("Joining...");

			UGameplayStatics::OpenLevel(this->GetWorld(), FName(*json->GetStringField("host")));

			// Clear widgets
			this->ClearViewport();

			UE_LOG(LogTemp, Warning, TEXT("CONNECTED!"));
		});
		
	}
	else if (header.type == MessageUtils::JOIN_GAME_FAILED_RESPONSE) {
		AsyncTask(ENamedThreads::GameThread, [this]() {
			this->ClearViewport();
			USearchGameWidget *currentScreen = ChangeCurrentScreen<USearchGameWidget>(GetWorld(), MainMenuWidget);

			// Connect to the matchmaking service
			if (this->MMManager->isConnected()) {
				currentScreen->SetStatus(USearchGameWidget::SUCCEED);
			}
			else {
				currentScreen->SetStatus(USearchGameWidget::FAILED);
			}

			currentScreen->Button_Play->OnClicked.AddDynamic(this, &AMenuGameState::searchGame);
		});
	}
}

void AMenuGameState::ClearViewport()
{
	for (TObjectIterator<UUserWidget> itr; itr; ++itr) {
		UUserWidget* existingWidget = *itr;

		if (!existingWidget->GetWorld()) {
			continue;
		}
		else {
			existingWidget->RemoveFromParent();
		}
	}
}


