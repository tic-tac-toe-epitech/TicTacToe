// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <functional>
#include "GameData/GUI/SearchGameWidget.h"
#include "GameData/GUI/LoadingScreen.h"
#include "GameData/Network/MatchmakingManager.h"
#include "GameData/Network/MatchmakingServerManager.h"
#include "GameData/Configuration/ConfigurationLoader.h"
#include "GenericPlatform/GenericPlatformProcess.h"
#include "SocketSubsystem.h"
#include "Sockets.h"
#include "Networking.h"
#include "CoreMinimal.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/GameState.h"
#include "MenuGameState.generated.h"


UCLASS()
class TICTACTOE_API AMenuGameState : public AGameState
{
	GENERATED_BODY()

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShowMainScreen);

	AMenuGameState();

	TSubclassOf<USearchGameWidget> MainMenuWidget;
	TSubclassOf<ULoadingScreen> LoadingScreenWidget;

	UFUNCTION()
	void searchGame();

protected:
	virtual void BeginPlay() override;

	void ClearViewport();

	void readClientPacket(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json);

private:
	FSocket* ProxySocket;

	FGeneralStruct GeneralConfig;

	UPROPERTY()
		class UConfigurationLoader *ConfigurationLoader;

	UPROPERTY()
		class UMatchmakingManager* MMManager;

	UPROPERTY()
		class UMatchmakingServerManager* MMServerManager;
};
