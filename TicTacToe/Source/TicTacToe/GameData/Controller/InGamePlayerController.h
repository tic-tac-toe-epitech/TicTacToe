// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GameplayTags.h"
#include "InGame/AI/OrderManager/OrderManagerInclude.h"
#include "InGamePlayerController.generated.h"

class ISelectableInterface;
class AUnit;
class ABuilding;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_InGamePlayerController_ActorArray, AInGamePlayerController*, Controller, TArray<AActor*>, Actors);
/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGamePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void NotifyActorChannelFailure(UActorChannel* ActorChan) override;

protected:
	// Always executed AFTER the tick function.
	UFUNCTION(BlueprintNativeEvent)
		void ServerTick(float DeltaTime);
	virtual void ServerTick_Implementation(float DeltaTime);

	// Always executed AFTER the tick function.
	UFUNCTION(BlueprintNativeEvent)
		void ClientTick(float DeltaTime);
	virtual void ClientTick_Implementation(float DeltaTime);

protected:
	UPROPERTY(BlueprintReadWrite)
		AActor* ActorUnderCursor;

	UPROPERTY(BlueprintReadWrite)
		TArray<AActor*> SelectedActors;

	UFUNCTION(BlueprintCallable)
		void TryToGiveOrderToActors(const TArray<AActor*>& Actors, FOrderStruct Order);

private:
	UFUNCTION(Server, Reliable, WithValidation)
		void GiveOrderToActorsRPCS(const TArray<AActor*>& Actors, FOrderStruct Order);
	void GiveOrderToActorsRPCS_Implementation(const TArray<AActor*>& Actors, FOrderStruct Order);
	bool GiveOrderToActorsRPCS_Validate(const TArray<AActor*>& Actors, FOrderStruct Order) { return true; }

public:
	UFUNCTION(BlueprintCallable)
		void SelectActors(TArray<AActor*> ActorsToSelect, bool bClearCurrentSelection);

	UFUNCTION(BlueprintCallable)
		void UnselectActors(TArray<AActor*> ActorsToUnselect);

	UFUNCTION(BlueprintCallable)
		void ClearSelection();

	UFUNCTION(BlueprintPure)
		AActor* GetActorUnderCursor() { return ActorUnderCursor; }

	UFUNCTION(BlueprintPure)
		TArray<AActor*> GetSelectedActors() { return SelectedActors; }

	UFUNCTION(BlueprintPure)
		TArray<AUnit*> GetSelectedUnits();

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*> GetSelectedBuildings();

	UFUNCTION(BlueprintPure)
		TArray<AUnit*> GetSelectedUnitsOfType(FGameplayTag Type);

	UFUNCTION(BlueprintPure)
		TArray<ABuilding*> GetSelectedBuildingsOfType(FGameplayTag Type);

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
		FEvent_InGamePlayerController_ActorArray OnSelectionChanged;
};
