// Fill out your copyright notice in the Description page of Project Settings.


#include "UnitSpawner.h"
#include "InGame/Unit/Unit.h"

//UUnitSpawner::UUnitSpawner()
//{
//	//PointerMapUnits = TMap<FString, TSoftClassPtr<AUnit>>();
//	//PointerMapBuildings = TMap<FString, TSoftClassPtr<ABuilding>>();
//}

TSoftClassPtr<AUnit> UUnitSpawner::GetSoftClassPtrAUnit(FString nameOfClass)
{
	return *PointerMapUnits.Find(nameOfClass);
}

TSoftClassPtr<ABuilding> UUnitSpawner::GetSoftClassPtrABuilding(FString const & nameOfClass)
{
	return *PointerMapBuildings.Find(nameOfClass);
}
