// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/Actor.h"
#include "StartingPlayerActors.generated.h"

/**
 *
 */
UCLASS()
class TICTACTOE_API UStartingPlayerActors : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY()
		TArray<AActor *> player1Actors;
	UPROPERTY()
		TArray<AActor *> player2Actors;
	UPROPERTY()
		APlayerController *player1;
	UPROPERTY()
		APlayerController *player2;
};
