// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "InGame/Unit/Unit.h"
#include "InGame/Building/Building.h"
#include "UnitSpawner.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TICTACTOE_API UUnitSpawner : public UObject
{
	GENERATED_BODY()
	
public:
	//UUnitSpawner();

private:
	UPROPERTY(EditAnywhere)
	TMap<FString, TSoftClassPtr<AUnit>> PointerMapUnits;
	UPROPERTY(EditAnywhere)
	TMap<FString, TSoftClassPtr<ABuilding>> PointerMapBuildings;
	//UPROPERTY(EditAnywhere)
	//TSoftClassPtr<AUnit> Villager;
	//UPROPERTY(EditAnywhere)
	//TSoftClassPtr<ABuilding> Hearth;

public:
	TSoftClassPtr<AUnit> GetSoftClassPtrAUnit(FString nameOfClass);
	TSoftClassPtr<ABuilding> GetSoftClassPtrABuilding(FString const & nameOfClass);
};
