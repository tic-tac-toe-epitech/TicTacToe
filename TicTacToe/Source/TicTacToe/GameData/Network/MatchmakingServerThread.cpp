// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchmakingServerThread.h"

FMatchmakingServerWorker* FMatchmakingServerWorker::Runnable = NULL;


FMatchmakingServerWorker::FMatchmakingServerWorker(UWorld *world) : world(world)
{
	Thread = FRunnableThread::Create(this, TEXT("FPrimeNumberWorker"), 0, TPri_BelowNormal);
}


FMatchmakingServerWorker::FMatchmakingServerWorker()
{
	Thread = FRunnableThread::Create(this, TEXT("FPrimeNumberWorker"), 0, TPri_BelowNormal);
}


//Init
bool FMatchmakingServerWorker::Init()
{
	return true;
}

//Run
uint32 FMatchmakingServerWorker::Run()
{
	FString portArg;
	int port = 8091;
	if (FParse::Value(FCommandLine::Get(), TEXT("MM_PORT"), portArg))
	{
		port  = FCString::Atoi(*portArg);
	}

	//Create Socket
	FIPv4Endpoint Endpoint(FIPv4Address(0, 0, 0, 0), port);
	FSocket* ListenSocket = FTcpSocketBuilder(FString("GameStatusListener"))
		.AsReusable()
		.BoundToEndpoint(Endpoint)
		.Listening(8);

	//Set Buffer Size
	int32 NewSize = 0;
	ListenSocket->SetReceiveBufferSize(2 * 1024 * 1024, NewSize);

	//Remote address
	TSharedRef<FInternetAddr> RemoteAddress = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	bool Pending = false;

	UE_LOG(LogTemp, Warning, TEXT("LISTENING FOR MM CONNECTIONS ON: %s"), *Endpoint.ToString());

	while (!Pending) {
		ListenSocket->HasPendingConnection(Pending);

		if (Pending) {

			// New connection
			UE_LOG(LogTemp, Warning, TEXT("CONNECTION FOUND"))

			FSocket *ConnectionSocket = ListenSocket->Accept(*RemoteAddress, TEXT("GameStatus connection"));

			if (ConnectionSocket != NULL)
			{
				UE_LOG(LogTemp, Warning, TEXT("CONNECTION ACCEPTED"));

				ServerConnectionThread* connection = new ServerConnectionThread();
				connection->InitThread(ConnectionSocket, std::bind(&FMatchmakingServerWorker::receive, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
			}

			Pending = false;
		}
	}

	return 0;
}

void FMatchmakingServerWorker::receive(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json) {
	if (header.type == MessageUtils::GAME_STATUS_REQUEST) {
		TSharedPtr<FJsonObject> body = MakeShareable(new FJsonObject());
		
		int players = this->world->GetAuthGameMode()->GetNumPlayers();
		int listeningPort = this->world->URL.Port;

		UE_LOG(LogTemp, Warning, TEXT("NUMBER OF PLAYERS: %d"), players);
		UE_LOG(LogTemp, Warning, TEXT("LISTENING ON: %d"), listeningPort);

		// Fixed values
		body->SetNumberField("players", players);
		body->SetStringField("state", "WAITING");
		body->SetNumberField("port", listeningPort);


		MessageUtils::Send(connection, MessageUtils::GAME_STATUS_RESPONSE, body);
	}
}

//stop
void FMatchmakingServerWorker::Stop()
{
}

FMatchmakingServerWorker* FMatchmakingServerWorker::JoyInit(UWorld *world)
{
	if (!Runnable && FPlatformProcess::SupportsMultithreading())
	{
		Runnable = new FMatchmakingServerWorker(world);
	}
	return Runnable;
}


FMatchmakingServerWorker* FMatchmakingServerWorker::JoyInit()
{
	if (!Runnable && FPlatformProcess::SupportsMultithreading())
	{
		Runnable = new FMatchmakingServerWorker();
	}
	return Runnable;
}

void FMatchmakingServerWorker::EnsureCompletion()
{
	Stop();
	Thread->WaitForCompletion();
}

void FMatchmakingServerWorker::Shutdown()
{
	if (Runnable)
	{
		Runnable->EnsureCompletion();
		delete Runnable;
		Runnable = NULL;
	}
}

bool FMatchmakingServerWorker::IsThreadFinished()
{
	if (Runnable) return false;
	return true;
}
