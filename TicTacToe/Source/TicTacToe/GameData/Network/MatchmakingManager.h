// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Messages/MessageUtils.h"
#include "Networking.h"
#include "Sockets.h"
#include "EngineUtils.h"
#include "CoreMinimal.h"
#include "UObject/UObjectIterator.h"
#include "Containers/UnrealString.h"
#include "Kismet/GameplayStatics.h"
#include "GameData/Network/ServerConnectionThread.h"
#include "GameData/GUI/SearchGameWidget.h"
#include "MatchmakingManager.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API UMatchmakingManager : public UObject
{
	GENERATED_BODY()
	
public:
	enum ConnectionState {
		NOT_CONNECTED = 0,
		CONNECTED = 1,
		CONNECTING = 2,
		SEARCHING = 3,
		MATCH_FOUND = 4
	};

	typedef std::function<void(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json)> PacketHandler;

	/*
	Init the matchmaking service.
	*/
	void init(FIPv4Address host, int port, PacketHandler packetHandler);

	/*
	Start receiving packets from the matchmaking service.
	*/
	void receive(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json);

	/*
	Connects to the Matchmaking service.
	*/
	bool connect();

	/*
	Disconnects from the matchmaking service
	*/
	void disconnect();

	/*
	Disconnects from the matchmaking service
	*/
	bool isConnected();

	/*
	Sends a packet to start the match search
	*/
	UFUNCTION()
	void sendSearchMatch();

	/*
	Sends a packet to stop the match search
	*/
	UFUNCTION()
	void sendStopSearchMatch();


	UFUNCTION()
		void OnSearchGame() {
		UE_LOG(LogTemp, Warning, TEXT("CLICKED!"));
	}

	UFUNCTION()
		void OnFoundGame() {
		UE_LOG(LogTemp, Warning, TEXT("CLICKED!"));
	}

private:
	const UWorld* world;
	
	PacketHandler packetHandler;
	ConnectionState state;
	FSocket* socket;
	USearchGameWidget* widget;

	FIPv4Address MMHost;
	int MMPort;
};
