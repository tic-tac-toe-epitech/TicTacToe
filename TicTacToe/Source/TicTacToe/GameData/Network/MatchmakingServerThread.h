// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Messages/MessageUtils.h"
#include "GameData/Network/ServerConnectionThread.h"
#include "CoreMinimal.h"
#include "Sockets.h"
#include "Networking.h"
#include "Engine/World.h"
#include "GameFramework/GameModeBase.h"
#include "HAL/Runnable.h"
#include "HAL/RunnableThread.h"
#include "GenericPlatform/GenericPlatformAffinity.h"
#include "GenericPlatform/GenericPlatformProcess.h"

/**
 * 
 */
class TICTACTOE_API FMatchmakingServerWorker : public FRunnable
{
	/** Singleton instance, can access the thread any time via static accessor, if it is active! */
	static  FMatchmakingServerWorker* Runnable;

	/** Thread to run the worker FRunnable on */
	FRunnableThread* Thread;

public:
	FMatchmakingServerWorker();
	FMatchmakingServerWorker(UWorld *world);
	virtual ~FMatchmakingServerWorker() {}

	// Begin FRunnable interface.
	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();

	/** Function called when a packet is received from the proxy connection */
	void receive(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json);
	static FMatchmakingServerWorker* JoyInit(UWorld *world);
	static FMatchmakingServerWorker* JoyInit();

	/** Shuts down the thread. Static so it can easily be called from outside the thread context */
	static void Shutdown();

	static bool IsThreadFinished();
	void EnsureCompletion();

private:
	UWorld *world;

};
