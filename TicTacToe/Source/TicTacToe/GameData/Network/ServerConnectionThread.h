// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Messages/MessageUtils.h"
#include "Sockets.h"
#include "Networking.h"
#include "Serialization/JsonSerializer.h"
#include "HAL/Runnable.h"
#include "HAL/RunnableThread.h"
#include "GenericPlatform/GenericPlatformAffinity.h"
#include "GenericPlatform/GenericPlatformProcess.h"
#include "CoreMinimal.h"
#include <functional>

class TICTACTOE_API ServerConnectionThread : public FRunnable
{
public:
	ServerConnectionThread();
	~ServerConnectionThread();

	//FRunnable interface.
	bool InitThread(FSocket *socket, std::function<void(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json)> callback);
	virtual uint32 Run();
	virtual void Stop();

private:
	FSocket* m_connection;
	std::function<void(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json)> m_callback;
	

	//Thread to run the worker FRunnable on
	FRunnableThread* Thread;

	FCriticalSection m_mutex;
	FEvent* m_semaphore;
	FThreadSafeBool m_kill;
};
