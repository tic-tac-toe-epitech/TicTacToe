// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Networking.h"
#include "Tickable.h"
#include "Sockets.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameData/Network/MatchmakingServerThread.h"
#include "MatchmakingServerManager.generated.h"

UCLASS()
class TICTACTOE_API UMatchmakingServerManager : public UObject, public FTickableGameObject
{
	GENERATED_BODY()

public:

	UFUNCTION()
	void init(UWorld* world);

	void Tick(float DeltaSeconds) override;
	bool IsTickable() const override;
	bool IsTickableInEditor() const override;
	bool IsTickableWhenPaused() const override;
	TStatId GetStatId() const override;
	UWorld* GetWorld() const override;

private:
	FSocket* socket;
	UWorld* world;
	bool tickable;
};
