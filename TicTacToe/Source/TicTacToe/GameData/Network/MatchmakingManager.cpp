// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchmakingManager.h"

void UMatchmakingManager::init(FIPv4Address mmHost, int mmPort, PacketHandler packetHandler) {
	this->state = ConnectionState::NOT_CONNECTED;

	this->MMHost = mmHost;
	this->MMPort = mmPort;
	this->packetHandler = packetHandler;
}

void UMatchmakingManager::receive(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json) {
	// Call 

	//if (header.type == MessageUtils::JOIN_GAME_RESPONSE) {
	//	// Try to connect to the game server

	//	UE_LOG(LogTemp, Warning, TEXT("CONNECTION TO %s"), *json->GetStringField("host"));

	//	UGameplayStatics::OpenLevel(this->world, FName(*json->GetStringField("host")));

	//	// Clear widgets
	//	for (TObjectIterator<UUserWidget> itr; itr; ++itr) {
	//		UUserWidget* existingWidget = *itr;

	//		if (!existingWidget->GetWorld()) {
	//			continue;
	//		}
	//		else {
	//			existingWidget->RemoveFromParent();
	//		}
	//	}

	//	UE_LOG(LogTemp, Warning, TEXT("CONNECTED!"));
	//}
	//else if (header.type == MessageUtils::JOIN_GAME_RESPONSE) {
	//	// Go back to the main screen


	//}
}

void UMatchmakingManager::disconnect() {
	socket->Close();
}

bool UMatchmakingManager::connect() {
	this->socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);

	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(this->MMHost.Value);
	addr->SetPort(this->MMPort);

	this->state = ConnectionState::CONNECTING;
	if (socket->Connect(*addr)) {
		ServerConnectionThread* proxyConnection = new ServerConnectionThread();
		proxyConnection->InitThread(this->socket, this->packetHandler);
		this->state = ConnectionState::CONNECTED;
		return true;
	}
	this->state = ConnectionState::NOT_CONNECTED;
	return false;
}

void UMatchmakingManager::sendSearchMatch() {
	if (this->socket->GetConnectionState() != ESocketConnectionState::SCS_Connected) {
		if (!this->connect()) {
			return;
		}
	}

	if (MessageUtils::Send(this->socket, MessageUtils::START_SEARCH_MATCH)) {
		this->state = ConnectionState::SEARCHING;
	}
}

void UMatchmakingManager::sendStopSearchMatch() {
	if (this->socket->GetConnectionState() != ESocketConnectionState::SCS_Connected) {
		return;
	}

	MessageUtils::Send(this->socket, MessageUtils::STOP_SEARCH_MATCH);
}

bool UMatchmakingManager::isConnected() {
	return (this->state == CONNECTED);
}