// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerConnectionThread.h"

ServerConnectionThread::ServerConnectionThread() {

}

ServerConnectionThread::~ServerConnectionThread()
{
	if (m_semaphore)
	{
		//Cleanup the FEvent
		FGenericPlatformProcess::ReturnSynchEventToPool(m_semaphore);
		m_semaphore = nullptr;
	}

	if (Thread)
	{
		//Cleanup the worker thread
		delete Thread;
		Thread = nullptr;
	}
}

bool ServerConnectionThread::InitThread(FSocket* connectionSocket, std::function<void(FSocket* connection, MessageUtils::MessageHeader header, TSharedPtr<FJsonObject> json)> callback) {
	this->m_connection = connectionSocket;
	this->m_callback = callback;
	this->m_kill = false;

	Thread = FRunnableThread::Create(this, TEXT("ConnectionThread"), 0, TPri_BelowNormal);
	return true;
}

uint32 ServerConnectionThread::Run() {
	UE_LOG(LogTemp, Warning, TEXT("START READING"));

	while (!m_kill)
	{
		uint32 size = 0;
		TArray<uint8> ReceivedData;

		while (this->m_connection->HasPendingData(size)) {
			ReceivedData.Init(FMath::Min(size, 65507u), size);

			int32 Read = 0;
			this->m_connection->Recv(ReceivedData.GetData(), ReceivedData.Num(), Read);
		}

		if (ReceivedData.Num() > 0) {
			UE_LOG(LogTemp, Warning, TEXT("READ! %d bytes"), ReceivedData.Num());

			MessageUtils::MessageHeader header;
			TSharedPtr<FJsonObject> body = MakeShareable(new FJsonObject());

			memcpy(&header, ReceivedData.GetData(), sizeof(MessageUtils::MessageHeader));

			UE_LOG(LogTemp, Warning, TEXT("PACKET TYPE %d"), header.type);
			UE_LOG(LogTemp, Warning, TEXT("PACKET LENGTH %d"), header.length);

			// Read body
			if (header.length) {
				ReceivedData.RemoveAt(0, 4, true);
				FString jsonBody;

				for (auto& Str : ReceivedData)
				{
					jsonBody += (char) Str;
				}

				// Deserialize JSON
				TSharedRef<TJsonReader<>> jsonReader = TJsonReaderFactory<>::Create(*jsonBody);
				FJsonSerializer::Deserialize(jsonReader, body);
			}

			this->m_callback(this->m_connection, header, body);
		}
		
	}

	UE_LOG(LogTemp, Warning, TEXT("CONNECTION ENDED!"))

	this->m_connection->Close();
	return 0;
}

void ServerConnectionThread::Stop() {
	this->m_kill = true;
	if (this->m_semaphore)
	{
		m_semaphore->Trigger();
	}
}
