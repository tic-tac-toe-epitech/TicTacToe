// Fill out your copyright notice in the Description page of Project Settings.


#include "MatchmakingServerManager.h"


void UMatchmakingServerManager::init(UWorld* newWorld) {
	this->world = newWorld;

	this->tickable = true;

	FMatchmakingServerWorker::JoyInit(newWorld);

	UE_LOG(LogTemp, Warning, TEXT("STARTED"));
}

void UMatchmakingServerManager::Tick(float DeltaSeconds) {
}

bool UMatchmakingServerManager::IsTickable() const
{
	return this->tickable;
}

bool UMatchmakingServerManager::IsTickableInEditor() const
{
	return false;
}

bool UMatchmakingServerManager::IsTickableWhenPaused() const
{
	return false;
}

TStatId UMatchmakingServerManager::GetStatId() const
{
	return TStatId();
}

UWorld* UMatchmakingServerManager::GetWorld() const {
	return this->world;
}