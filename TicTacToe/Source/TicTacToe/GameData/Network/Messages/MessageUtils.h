// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Sockets.h"
#include "SocketSubsystem.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"


class TICTACTOE_API MessageUtils
{
public:
	struct MessageHeader {
		uint16 length;
		uint16 type;
	};

	enum MessageType {
		UNKNOWN_PACKET = 0,
		START_SEARCH_MATCH = 1,
		STOP_SEARCH_MATCH = 2,
		GAME_STATUS_REQUEST = 3,
		GAME_STATUS_RESPONSE = 4,
		JOIN_GAME_RESPONSE = 5,
		JOIN_GAME_FAILED_RESPONSE = 6
	};

	static bool Send(FSocket *connection, MessageType type, TSharedPtr<FJsonObject> body) {
		MessageUtils::MessageHeader header;
		FString json;
		int sent;

		TSharedRef<TJsonWriter<TCHAR>> writer = TJsonWriterFactory<TCHAR>::Create(&json);
		FJsonSerializer::Serialize(body.ToSharedRef(), writer);

		TCHAR *jsonRaw = json.GetCharArray().GetData();

		header.length = FCString::Strlen(jsonRaw);
		header.type = type;

		int size = header.length;
		uint8 *buff = new uint8[size + sizeof(header)];

		memcpy(buff, &header, sizeof(header));
		memcpy(buff + sizeof(header), (uint8*)TCHAR_TO_UTF8(jsonRaw), size);

		return connection->Send(buff, size + sizeof(header), sent);
	}

	static bool Send(FSocket *connection, MessageType type) {
		MessageUtils::MessageHeader header;
		int sent;

		header.length = 0;
		header.type = type;

		int size = header.length;

		return connection->Send((uint8 *) &header, sizeof(header), sent);
	}
	
};
