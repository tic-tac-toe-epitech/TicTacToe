// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "InGameGameMode.generated.h"

class AUnit;
class ABuilding;
class UUnitSpawner;
class UStartingPlayerActors;
class UMapGenerator;

UCLASS()
class TICTACTOE_API AInGameGameMode : public AGameMode
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	AUnit* SpawnNewUnit(TSoftClassPtr<AUnit> UnitClass, APlayerController* OwningPlayer, FTransform UnitTransform);

	UPROPERTY()
		class UMatchmakingManager* mmManager;

	UPROPERTY()
		class UMatchmakingServerManager* mmServerManager;

	ABuilding* SpawnNewBuilding(TSoftClassPtr<ABuilding> BuildingClass, APlayerController* OwningPlayer, FTransform BuildingTransform);

	UPROPERTY(EditAnywhere)
	TSoftClassPtr<UUnitSpawner> BP_UUnitSpawnerClass;

	UFUNCTION()
		void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;

	UFUNCTION()
	void SpawnBaseUnitsForPlayers();
private:
	UFUNCTION()
		void OnReceivePlayersStart(UMapGenerator *MapGenerator);
private:
	APlayerController* Player1 = nullptr;
	APlayerController* Player2 = nullptr;
	UPROPERTY()
	UStartingPlayerActors* playerActors;
	UPROPERTY()
	UMapGenerator *MG;
	UPROPERTY()
	UUnitSpawner *US;
	bool TwoPlayersConnected = false;
	bool PlayerStartGathered = false;
};
