// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameData/GUI/SearchGameWidget.h"
#include "GameData/Network/MatchmakingManager.h"
#include "CoreMinimal.h"
#include "Sockets.h"
#include "GameFramework/GameMode.h"
#include "MenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API AMenuGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:

protected:
	virtual void BeginPlay() override;

};
