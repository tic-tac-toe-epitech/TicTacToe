// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameGameMode.h"
#include "InGame/MapGenerator/MapGenerator.h"
#include "GameData/Network/MatchmakingManager.h"
#include "GameData/Network/MatchmakingServerManager.h"
#include "InGame/Unit/Unit.h"
#include "InGame/Building/Building.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "GameFramework/PlayerState.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "GameData/UnitSpawner/UnitSpawner.h"
#include "GameData/UnitSpawner/StartingPlayerActors.h"

void AInGameGameMode::BeginPlay()
{
	Super::BeginPlay();
	this->playerActors = NewObject<UStartingPlayerActors>();
	US = NewObject<UUnitSpawner>(this, BP_UUnitSpawnerClass.LoadSynchronous());
	MG = NewObject<UMapGenerator>();
	//MG->OnPlayerStartGathered.AddDynamic(this, &AInGameGameMode::OnReceivePlayersStart);
	MG->Init(US);
	MG->GenerateMap(this, GetRootComponent());

	if (this->GetWorld()->GetNetMode() == NM_DedicatedServer) {
		this->mmServerManager = NewObject<UMatchmakingServerManager>();
		this->mmServerManager->init(this->GetWorld());
	}
}

void AInGameGameMode::OnReceivePlayersStart(UMapGenerator * MapGenerator)
{
	MapGenerator->OnPlayerStartGathered.RemoveDynamic(this, &AInGameGameMode::OnReceivePlayersStart);
	PlayerStartGathered = true;
	if (TwoPlayersConnected)
	{
		SpawnBaseUnitsForPlayers();
	}
}

AUnit* AInGameGameMode::SpawnNewUnit(TSoftClassPtr<AUnit> UnitClass, APlayerController* OwningPlayer, FTransform UnitTransform)
{
 	AUnit* NewUnit = nullptr;
	FActorSpawnParameters params;
	if (OwningPlayer)
	{
		params.Owner = OwningPlayer->GetPlayerState<APlayerState>();
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		NewUnit = GetWorld()->SpawnActor<AUnit>(UnitClass.LoadSynchronous(), UnitTransform, params);
	}

	return NewUnit;
}

ABuilding* AInGameGameMode::SpawnNewBuilding(TSoftClassPtr<ABuilding> BuildingClass, APlayerController* OwningPlayer, FTransform BuildingTransform)
{
	ABuilding* NewBuilding = nullptr;
	FActorSpawnParameters params;
	if (OwningPlayer)
	{
		params.Owner = OwningPlayer->GetPlayerState<APlayerState>();
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		NewBuilding = GetWorld()->SpawnActor<ABuilding>(BuildingClass.LoadSynchronous(), BuildingTransform, params);
	}
	return NewBuilding;
}

void AInGameGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);

	if (Player1 == nullptr)
	{
		Player1 = NewPlayer;
	}
	else
	{
		Player2 = NewPlayer;
		TwoPlayersConnected = true;
		SpawnBaseUnitsForPlayers();
	}
}

void AInGameGameMode::SpawnBaseUnitsForPlayers()
{
	playerActors = MG->GetArrayPlayersStartingActors();
	playerActors->player1 = Player1;
	playerActors->player2 = Player2;
	MG->SpawnAllTheThings(GetRootComponent(), playerActors);
}
