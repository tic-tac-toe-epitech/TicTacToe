// Fill out your copyright notice in the Description page of Project Settings.


#include "ResourcePoint.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AResourcePoint::AResourcePoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	bAlwaysRelevant = true;

	ResourceMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ressource Mesh Component"));
	ResourceMesh->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));
}

// Called when the game starts or when spawned
void AResourcePoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AResourcePoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

