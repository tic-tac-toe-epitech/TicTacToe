// Fill out your copyright notice in the Description page of Project Settings.


#include "MapGenerator.h"
//#include "DateTime.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/LevelStreaming.h"
#include "Math/Vector.h"
#include "Misc/Paths.h" 
#include "HAL/FileManager.h"
#include "Logging/MessageLog.h"
#include "Components/BoxComponent.h"
#include "UObject/UObjectGlobals.h"
#include "UObject/SoftObjectPtr.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerStart.h"
#include "GameData/GameMode/InGameGameMode.h"
#include "GameData/UnitSpawner/UnitSpawner.h"
#include "GameData/UnitSpawner/StartingPlayerActors.h"
#include "TicTacToe.h"


//TODO: Constants put in gameplay statics

void UMapGenerator::Init(UUnitSpawner *US)
{
	this->US = US;
}

void UMapGenerator::GenerateMap(UObject* worldContextObject, USceneComponent *rootComponent)
{
	const int32 squaresPerSides = 3;
	TArray<FName> ListOfNameLevels = GetNameOfLevels(true);
	TArray<FName> ListOfMiddleLevels = GetNameOfCentralLevels(true);
	int32 indexMap = 0;
	//TODO: shuffle array, pour pas qu'il y ait de doublons
	for (int i = 0; i < 9; i++)
	{
		FLatentActionInfo	info;
		ULevelStreaming		*subLevel = nullptr;
		int32				hasError = false;
		//4 is middle tile
		if (i == 4)
		{
			indexMap = UKismetMathLibrary::RandomIntegerInRange(0, 2);
			UGameplayStatics::LoadStreamLevel(worldContextObject, ListOfMiddleLevels[indexMap], false, false, info);
			subLevel = UGameplayStatics::GetStreamingLevel(worldContextObject, ListOfMiddleLevels[indexMap]);
			if (subLevel == NULL)
			{
				FMessageLog("Map Generation").Info(FText::FromString(TEXT("Error getting streaming level for side zones")));
				return;
			}
		}
		else
		{
			indexMap = UKismetMathLibrary::RandomIntegerInRange(0, 8);
			UGameplayStatics::LoadStreamLevel(worldContextObject, ListOfNameLevels[indexMap], false, false, info);
			subLevel = UGameplayStatics::GetStreamingLevel(worldContextObject, ListOfNameLevels[indexMap]);
			if (subLevel == NULL)
			{
				FMessageLog("Map Generation").Info(FText::FromString(TEXT("Error getting streaming level for middle zone")));
				return;
			}
		}
		subLevel->LevelTransform = CreateTransformForSubLevel(i);
		subLevel->SetShouldBeLoaded(true);
		subLevel->SetShouldBeVisible(true);
	}
	GenerateMapWalls();
}

FString const UMapGenerator::GetUniqueInstanceMapName(int32 tileIterator)
{
	FString FStringMap = FString(TEXT("Map"));
	FString uniqueIterator = FString::FromInt(tileIterator);
	FString uniqueInstanceName = FStringMap + uniqueIterator;
	return uniqueInstanceName;
}

FTransform UMapGenerator::CreateTransformForSubLevel(int index)
{
	float x = (((index % 3) - 1) * TILESIZE) * VERTEXVALUE;
	float y = (((index / 3) - 1) * TILESIZE) * VERTEXVALUE;
	FTransform transformVector;
	if (index == 4)
	{
		transformVector = FTransform(FRotator(0.f, 90.f, 0.f), FVector(x, y, 0.f));
	}
	else
	{
		transformVector = FTransform(FVector(x, y, 0.f));
	}
	return transformVector;
}

TArray<FName> UMapGenerator::GetNameOfLevels(bool isExample) const
{
	TArray<FName> ret = TArray<FName>({ "1Map", "2Map", "3Map", "4Map", "5Map", "6Map" , "7Map" , "8Map" , "9Map" });
	return ret;
}

TArray<FName> UMapGenerator::GetNameOfCentralLevels(bool isExample) const
{
	TArray<FName> ret = TArray<FName>({ "1CenterMap", "2CenterMap", "3CenterMap" });
	return ret;
}

int32 UMapGenerator::GenerateMapWalls()
{
	return false;
}

void UMapGenerator::SpawnBaseUnitForPlayer(APlayerController * playerController, UObject* worldContextObject, FString nameOfActor, AActor *playerStart)
{
	AInGameGameMode *ptrIngameGameMode = Cast<AInGameGameMode>(UGameplayStatics::GetGameMode(worldContextObject));
	FTransform transformVector = playerStart->GetTransform();
	TSoftClassPtr<AUnit> softclassUnit = US->GetSoftClassPtrAUnit(nameOfActor);
	ptrIngameGameMode->SpawnNewUnit(softclassUnit, playerController, transformVector);
}

void UMapGenerator::SpawnBaseBuildingForPlayer(APlayerController * playerController, UObject * worldContextObject, FString nameOfActor, AActor * playerStart)
{
	AInGameGameMode *ptrIngameGameMode = Cast<AInGameGameMode>(UGameplayStatics::GetGameMode(worldContextObject));
	FTransform transformVector = playerStart->GetTransform();
	TSoftClassPtr<ABuilding> softclassBuilding = US->GetSoftClassPtrABuilding(nameOfActor);
	ptrIngameGameMode->SpawnNewBuilding(softclassBuilding, playerController, transformVector);
}

UStartingPlayerActors *UMapGenerator::GetArrayPlayersStartingActors()
{
	UStartingPlayerActors *funradio = NewObject<UStartingPlayerActors>();

	funradio->player1Actors = player1StartingActors;
	funradio->player2Actors = player2StartingActors;

	return funradio;
}

void UMapGenerator::SpawnAllTheThings(UObject *WorldContextObject, UStartingPlayerActors *playerActors)
{
	TArray<AActor*> AllPlayerStart;

	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, APlayerStart::StaticClass(), AllPlayerStart);

	for (auto playerStart : AllPlayerStart)
	{
		APlayerController* player = nullptr;

		if (playerStart->ActorHasTag("Player1"))
		{
			player = playerActors->player1;
		}
		else if (playerStart->ActorHasTag("Player2"))
		{
			player = playerActors->player2;
		}

		if (player)
		{
			//TODO: Should be worker, to debug
			if (playerStart->ActorHasTag("Villager"))
			{
				this->SpawnBaseUnitForPlayer(player, WorldContextObject, FString(TEXT("Worker")), playerStart);
			}
			else if (playerStart->ActorHasTag("CityHall"))
			{
				this->SpawnBaseBuildingForPlayer(player, WorldContextObject, FString(TEXT("CityHall")), playerStart);
			}
			//TODO: Should be Heart, to debug
			else if (playerStart->ActorHasTag("Hearth"))
			{
				this->SpawnBaseBuildingForPlayer(player, WorldContextObject, FString(TEXT("Heart")), playerStart);
			}
			else
			{
				UE_LOG(TicTacLogMapGeneration, Warning,
					TEXT("UMapGenerator::SpawnAllTheThings: Couldn't spawn the actor for player start '%s', the tag was not known or present"),
					*GetNameSafe(playerStart));
			}
		}
		else
		{
			UE_LOG(TicTacLogMapGeneration, Log,
				TEXT("UMapGenerator::SpawnAllTheThings: Player start '%s' has neither 'Player1' nor 'Player2' tag. Cannot spawn anything on it."),
				*GetNameSafe(playerStart));
		}
	}
}

void UMapGenerator::FillArraysWithStartingPlayers()
{
	//iterator_diablo is used because for some reason all the actors are loaded when this function is called the second time only. FUCK UNREAL ENGINE

	if (iterator_diablo == 0)
	{
		iterator_diablo++;
	}
	else if (iterator_diablo == 1)
	{
		TArray<AActor *> AllPlayerStart;

		UGameplayStatics::GetAllActorsOfClass(currentLevelStreaming, APlayerStart::StaticClass(), AllPlayerStart);
		for (int32 i = 0; i < 7; ++i)
		{
			player1StartingActors.Add(AllPlayerStart[i]);
		}
		for (int32 i = 7; i < 14; ++i)
		{
			player2StartingActors.Add(AllPlayerStart[i]);
		}
		OnPlayerStartGathered.Broadcast(this);
	}
	else
	{
		UE_LOG(TicTacLogMapGeneration, Warning, TEXT("UMapGenerator::FillArraysWithStartingPlayers: ya une couille dans le pate dans la generation de map"));
	}
}