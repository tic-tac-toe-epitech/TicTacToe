// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MapGenerator.generated.h"

class UUnitSpawner;
class UStartingPlayerActors;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEvent_UMapGenerator, UMapGenerator*, MapGenerator);

/**
 *
 */
UCLASS()
class TICTACTOE_API UMapGenerator : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FEvent_UMapGenerator OnPlayerStartGathered;
	UFUNCTION(BlueprintCallable)
	void GenerateMap(UObject* WorldContextObject, USceneComponent *rootComponent);
	UFUNCTION()
	void Init(UUnitSpawner * US);
	UFUNCTION()
	UStartingPlayerActors *GetArrayPlayersStartingActors();
	UFUNCTION()
	void SpawnAllTheThings(UObject *WorldContextObject, UStartingPlayerActors *playerActors);
private:
	UFUNCTION()
	FString const GetUniqueInstanceMapName(int32 tileIterator);
	UFUNCTION()
	TArray<FName> GetNameOfLevels(bool isExample) const;
	UFUNCTION()
	FTransform CreateTransformForSubLevel(int index);
	UFUNCTION()
	TArray<FName> GetNameOfCentralLevels(bool isExample) const;
	UFUNCTION()
	int32 GenerateMapWalls();
	UFUNCTION()
	void FillArraysWithStartingPlayers();
	UFUNCTION()
	void SpawnBaseUnitForPlayer(APlayerController *playerController, UObject* worldContextObject, FString nameOfActor, AActor *playerStart);
	UFUNCTION()
	void SpawnBaseBuildingForPlayer(APlayerController *playerController, UObject* worldContextObject, FString nameOfActor, AActor *playerStart);

	//TArray<AActor *> *GetPlayer1StartingActors();
	//TArray<AActor *> *GetPlayer2StartingActors();


private:
	const float VERTEXVALUE = 100.f;
	const float TILESIZE = 100.f;
		int32 iterator_diablo = 0;
//for map generation
private:
	UUnitSpawner *US;
	ULevelStreaming *currentLevelStreaming;
	UPROPERTY()
	TArray<AActor *> player1StartingActors;
	UPROPERTY()
	TArray<AActor *> player2StartingActors;
};
