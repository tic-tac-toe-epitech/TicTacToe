// Fill out your copyright notice in the Description page of Project Settings.


#include "SelectionDecalComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Kismet/GameplayStatics.h"
#include "InGame/Diplomacy/DiplomacyInclude.h"
#include "TicTacToe.h"

USelectionDecalComponent::USelectionDecalComponent() : Super()
{
	PrimaryComponentTick.bCanEverTick = false;

	// Decals are applied vertically, so we rotate it to make it face the ground.
	SetRelativeRotation(FRotator(90, 0, 0));
	DecalSize = FVector(5, 35, 35);

	bOnlyVisibleWhenSelectedOrHovered = false;

	// Translucid green
	NormalColorOwned = FLinearColor(0, 0.1, 0, 0.75);
	
	// Almost opaque green
	SelectedColorOwned = FLinearColor::Green;

	HoveredColorOwned = FLinearColor::Yellow;

	// Translucid red
	NormalColorEnemy = FLinearColor(0.1, 0, 0, 0.75);

	// Almost opaque red
	SelectedColorEnemy = FLinearColor::Red;

	HoveredColorEnemy = FLinearColor::Yellow;

	// Translucid gray
	NormalColorNeutral = FLinearColor(0.1f, 0.1f, 0.1f, 0.75);

	// Almost opaque gray
	SelectedColorNeutral = FLinearColor::Gray;

	HoveredColorNeutral = FLinearColor::Yellow;
}

void USelectionDecalComponent::BeginPlay()
{
	Super::BeginPlay();
	CreateDynamicMaterial();
	if (bOnlyVisibleWhenSelectedOrHovered)
	{
		SetHiddenInGame(false);
	}
}

void USelectionDecalComponent::CreateDynamicMaterial()
{
	if (!DynamicMaterial)
	{
		DynamicMaterial = UMaterialInstanceDynamic::Create(GetDecalMaterial(), this);
		SetDecalMaterial(DynamicMaterial);
	}
}

void USelectionDecalComponent::SetDecalColor(FLinearColor& Color, float Glow)
{
	CreateDynamicMaterial();
	if (DynamicMaterial)
	{
		DynamicMaterial->SetVectorParameterValue(TEXT("Color"), Color);
		DynamicMaterial->SetScalarParameterValue(TEXT("Glow"), Glow);
	}
	else
	{
		UE_LOG(TicTacLogSelection,
			Warning,
			TEXT("USelectionDecalComponent::SetDecalColor: Trying to set decal color but no dynamic material instance exists. Self: '%s', Parent actor: '%s'."),
			*GetNameSafe(this), *GetNameSafe(GetAttachmentRootActor()))
	}
}

void USelectionDecalComponent::SetDecalSize(float InSize)
{
	DecalSize = FVector(5, InSize, InSize);

	CreateDynamicMaterial();
	if (DynamicMaterial)
	{
		DynamicMaterial->SetScalarParameterValue(TEXT("DecalSize"), InSize);
	}
}

void USelectionDecalComponent::SetIsSelected(bool IsSelected, EDiplomacy Diplomacy)
{
	bIsSelected = IsSelected;

	if (bIsHovered)
	{
		SetIsHovered(bIsHovered, Diplomacy);
		return;
	}
	if (bOnlyVisibleWhenSelectedOrHovered)
	{
		SetHiddenInGame(!bIsSelected, false);
	}
	float glow = bIsSelected ? 1.5 : 2;
	switch (Diplomacy)
	{
	case EDiplomacy::OWNED:
		SetDecalColor(bIsSelected ? SelectedColorOwned : NormalColorOwned, glow);
		break;

	case EDiplomacy::ENEMY:
		SetDecalColor(bIsSelected ? SelectedColorEnemy : NormalColorEnemy, glow);
		break;

	case EDiplomacy::NEUTRAL:
		SetDecalColor(bIsSelected ? SelectedColorNeutral : NormalColorNeutral, glow);
		break;

	default:
		SetDecalColor(bIsSelected ? SelectedColorNeutral : NormalColorNeutral, glow);
	};
}

void USelectionDecalComponent::SetIsHovered(bool IsHovered, EDiplomacy Diplomacy)
{
	bIsHovered = IsHovered;

	if (bIsHovered)
	{

		if (bOnlyVisibleWhenSelectedOrHovered)
		{
			SetHiddenInGame(false, false);
		}
		switch (Diplomacy)
		{
		case EDiplomacy::OWNED:
			SetDecalColor(HoveredColorOwned, 2);
			break;

		case EDiplomacy::ENEMY:
			SetDecalColor(HoveredColorEnemy, 2);
			break;

		case EDiplomacy::NEUTRAL:
			SetDecalColor(HoveredColorNeutral, 2);
			break;

		default:
			SetDecalColor(HoveredColorNeutral, 2);
		};
	}
	else
	{
		SetIsSelected(bIsSelected, Diplomacy);
	}
}