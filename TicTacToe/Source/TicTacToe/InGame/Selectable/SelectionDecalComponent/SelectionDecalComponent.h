// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/DecalComponent.h"
#include "InGame/Diplomacy/DiplomacyInclude.h"
#include "SelectionDecalComponent.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class TICTACTOE_API USelectionDecalComponent : public UDecalComponent
{
	GENERATED_BODY()
		USelectionDecalComponent();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		bool bOnlyVisibleWhenSelectedOrHovered;

	UPROPERTY(EditAnywhere)
		FLinearColor NormalColorOwned;

	UPROPERTY(EditAnywhere)
		FLinearColor HoveredColorOwned;

	UPROPERTY(EditAnywhere)
		FLinearColor SelectedColorOwned;

	UPROPERTY(EditAnywhere)
		FLinearColor NormalColorEnemy;

	UPROPERTY(EditAnywhere)
		FLinearColor HoveredColorEnemy;

	UPROPERTY(EditAnywhere)
		FLinearColor SelectedColorEnemy;

	UPROPERTY(EditAnywhere)
		FLinearColor NormalColorNeutral;

	UPROPERTY(EditAnywhere)
		FLinearColor HoveredColorNeutral;

	UPROPERTY(EditAnywhere)
		FLinearColor SelectedColorNeutral;

	UPROPERTY()
		UMaterialInstanceDynamic* DynamicMaterial;

	UFUNCTION()
		void CreateDynamicMaterial();

private:
	bool bIsSelected;
	bool bIsHovered;

	void SetDecalColor(FLinearColor& Color, float Glow);

public:
	UFUNCTION(BlueprintCallable)
		void SetDecalSize(float InSize);

	UFUNCTION()
		void SetIsSelected(bool IsSelected, EDiplomacy Diplomacy);

	UFUNCTION()
		void SetIsHovered(bool IsHovered, EDiplomacy Diplomacy);
};
