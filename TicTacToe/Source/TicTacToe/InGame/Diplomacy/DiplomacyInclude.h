#pragma once

#include "CoreMinimal.h"
#include "DiplomacyInclude.generated.h"

UENUM(BlueprintType)
enum class EDiplomacy : uint8
{
	OWNED,
	NEUTRAL,
	ENEMY
};