// Fill out your copyright notice in the Description page of Project Settings.


#include "Building.h"
#include "TicTacToe.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "InGame/Selectable/SelectionDecalComponent/SelectionDecalComponent.h"
#include "Libraries/DiplomacyLibrary.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "Engine/StaticMesh.h"

// STATICS

TArray<ABuilding*> ABuilding::GetBuildingsOfType(TArray<ABuilding*> BuildingsToFiler, FGameplayTag Type)
{
	TArray<ABuilding*> ret;

	if (!Type.MatchesTag(FGameplayTag::RequestGameplayTag("FactionEntity.Building")))
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Warning,
			TEXT("ABuilding::GetBuildingsOfType : Given type does not match tag 'FactionEntity.Building'. Given type : '%s'."),
			*Type.ToString());
	}
	else
	{
		for (auto building : BuildingsToFiler)
		{
			if (building->GetBuildingType().MatchesTag(Type))
			{
				ret.AddUnique(building);
			}
		}
	}
	return ret;
}

// END STATICS


// Sets default values
ABuilding::ABuilding() : Super()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = false;
	bReplicates = true;
	bAlwaysRelevant = true;

	BuildingMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Building Mesh"));
	RootComponent = BuildingMeshComponent;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	BoxComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));

	SquareDecalComponent = CreateDefaultSubobject<USelectionDecalComponent>(TEXT("Selection Decal Component"));
	SquareDecalComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));
}

bool ABuilding::ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	// Subobject replication sample. Replace ReplicatedSubobject by your subobject and repeat these steps for any other objects that need to be replicated.
	//if (ReplicatedSubobject != nullptr)
	//{
	//	WroteSomething |= Channel->ReplicateSubobject(ReplicatedSubobject, *Bunch, *RepFlags);
	//}

	return WroteSomething;
}

void ABuilding::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	//DOREPLIFETIME(ABuilding, ReplicatedProperty);
}

void ABuilding::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (BuildingMeshComponent->GetStaticMesh())
	{
		FVector meshSize = BuildingMeshComponent->GetStaticMesh()->GetBounds().GetBox().GetSize();
		BoxComponent->SetBoxExtent(meshSize / 2);
		BoxComponent->SetRelativeLocation(FVector(0, 0, meshSize.Z / 2));
		float sideSize = meshSize.Y < meshSize.X ? meshSize.Y : meshSize.X;
		SquareDecalComponent->SetDecalSize(sideSize);
	}
}

// Called when the game starts or when spawned
void ABuilding::BeginPlay()
{
	Super::BeginPlay();

	AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(GetOwner());
	if (ownerAsInGamePlayerState)
	{
		ownerAsInGamePlayerState->AddBuilding(this);
	}
	if (!HasAuthority())
	{
		SquareDecalComponent->SetIsSelected(Selected, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
	}
}

void ABuilding::OnRep_Owner()
{
	Super::OnRep_Owner();
	if (SquareDecalComponent)
	{
		SquareDecalComponent->SetIsSelected(Selected, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
	}
	AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(GetOwner());
	if (ownerAsInGamePlayerState)
	{
		ownerAsInGamePlayerState->AddBuilding(this);
	}
}

// Called every frame
void ABuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

#pragma region Gameplay Tags interface

void ABuilding::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.AddTag(BuildingType);
	TagContainer.AppendTags(GameplayTagContainer);
}

#pragma endregion Gameplay Tags interface

#pragma region Selectable interface

void ABuilding::Select_Implementation()
{
	Selected = true;

	SquareDecalComponent->SetIsSelected(true, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void ABuilding::Unselect_Implementation()
{
	Selected = false;

	SquareDecalComponent->SetIsSelected(false, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void ABuilding::Hover_Implementation()
{
	Hovered = true;

	SquareDecalComponent->SetIsHovered(true, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void ABuilding::Unhover_Implementation()
{
	Hovered = false;

	SquareDecalComponent->SetIsHovered(false, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

#pragma endregion Selectable interface
