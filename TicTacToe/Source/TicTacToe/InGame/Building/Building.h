// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InGame/Selectable/SelectableInterface.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"
#include "Building.generated.h"

class UStaticMeshComponent;
class USelectionDecalComponent;
class UBoxComponent;

UCLASS()
class TICTACTOE_API ABuilding : public AActor, public ISelectableInterface, public IGameplayTagAssetInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuilding();

	virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

	UFUNCTION(BlueprintPure)
		static TArray<ABuilding*> GetBuildingsOfType(TArray<ABuilding*> BuildingsToFilter, FGameplayTag Type);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnRep_Owner() override;

public:	
	virtual void PostInitializeComponents() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

#pragma region Components

protected:
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* BuildingMeshComponent;

	UPROPERTY(EditAnywhere)
		UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere)
		USelectionDecalComponent* SquareDecalComponent;

#pragma endregion Components

#pragma region Selectable Interface

private:
	bool Selected;
	bool Hovered;

public:
	virtual void Select_Implementation() override;

	virtual void Unselect_Implementation() override;

	virtual void Hover_Implementation() override;

	virtual void Unhover_Implementation() override;

#pragma endregion Selectable Interface

#pragma region GameplayTag Asset Interface

protected:
	UPROPERTY(EditAnywhere)
		FGameplayTag BuildingType;

	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;

	UPROPERTY(EditAnywhere)
		FGameplayTagContainer GameplayTagContainer;

public:
	UFUNCTION(BlueprintPure)
		FGameplayTag GetBuildingType() { return BuildingType; }

#pragma endregion GameplayTag Asset Interface
};
