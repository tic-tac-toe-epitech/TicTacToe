// Fill out your copyright notice in the Description page of Project Settings.


#include "Unit.h"
#include "GameplayTagContainer.h"
#include "TicTacToe.h"
#include "InGame/Selectable/SelectionDecalComponent/SelectionDecalComponent.h"
#include "Materials/MaterialInterface.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Kismet/GameplayStatics.h"
#include "Libraries/DiplomacyLibrary.h"
#include "GameFramework/GameMode.h"
#include "TimerManager.h"
#include "GameData/PlayerState/InGamePlayerState.h"
#include "Components/CapsuleComponent.h"
#include "Runtime/Engine/Public/WorldCollision.h"
#include "InGame/AI/OrderManager/UnitOrderManagerComponent.h"
#include "InGame/AI/Formation/Formation.h"

// STATICS

TArray<AUnit*> AUnit::GetUnitsOfType(TArray<AUnit*> UnitsToFiler, FGameplayTag Type)
{
	TArray<AUnit*> ret;

	if (!Type.MatchesTag(FGameplayTag::RequestGameplayTag("FactionEntity.Unit")))
	{
		UE_LOG(TicTacLogPlayerCivilization,
			Warning,
			TEXT("AUnit::GetUnitsOfType : Given type does not match tag 'FactionEntity.Unit'. Given type : '%s'."),
			*Type.ToString());
	}
	else
	{
		for (auto unit : UnitsToFiler)
		{
			if (unit->GetUnitType().MatchesTag(Type))
			{
				ret.AddUnique(unit);
			}
		}
	}
	return ret;
}

// END STATICS


// Sets default values
AUnit::AUnit() : Super()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bStartWithTickEnabled = false;
	bReplicates = true;
	bAlwaysRelevant = true;
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	CircleDecalComponent = CreateDefaultSubobject<USelectionDecalComponent>(TEXT("Selection Decal Component"));
	CircleDecalComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false));

	OrderManager = CreateDefaultSubobject<UUnitOrderManagerComponent>(TEXT("Order Manager Component"));
	OrderManager->SetIsReplicated(true);
}

bool AUnit::ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags)
{
	bool WroteSomething = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	// Subobject replication sample. Replace ReplicatedSubobject by your subobjects and repeat these steps for any objects that need to be replicated.
	//if (ReplicatedSubobject != nullptr)
	//{
	//	WroteSomething |= Channel->ReplicateSubobject(ReplicatedSubobject, *Bunch, *RepFlags);
	//}

	return WroteSomething;
}

void AUnit::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Property replication sample. Replace ReplicatedProperty by your property and repeat this line for any properties that need to be replicated.
	//DOREPLIFETIME(AUnit, ReplicatedProperty);
}

void AUnit::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	UCapsuleComponent* capsuleComponent = FindComponentByClass<UCapsuleComponent>();
	if (capsuleComponent)
	{
		float radius = capsuleComponent->GetCollisionShape().Capsule.Radius;
		float halfHeight = capsuleComponent->GetCollisionShape().Capsule.HalfHeight;
		CircleDecalComponent->SetDecalSize(radius);
		CircleDecalComponent->SetRelativeLocation(FVector(0, 0, -halfHeight));
	}
}

// Called when the game starts or when spawned
void AUnit::BeginPlay()
{
	Super::BeginPlay();

	AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(GetOwner());
	if (ownerAsInGamePlayerState)
	{
		ownerAsInGamePlayerState->AddUnit(this);
	}
	if (!HasAuthority())
	{
		CircleDecalComponent->SetIsSelected(Selected, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
	}

}

void AUnit::OnRep_Owner()
{
	Super::OnRep_Owner();
	if (CircleDecalComponent)
	{
		CircleDecalComponent->SetIsSelected(Selected, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
	}
	AInGamePlayerState* ownerAsInGamePlayerState = Cast<AInGamePlayerState>(GetOwner());
	if (ownerAsInGamePlayerState)
	{
		ownerAsInGamePlayerState->AddUnit(this);
	}
}


// Called every frame
void AUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AUnit::GetUnitRadius()
{
	UCapsuleComponent* capsuleComponent = FindComponentByClass<UCapsuleComponent>();

	if (capsuleComponent)
	{
		return capsuleComponent->GetCollisionShape().Capsule.Radius;
	}
	return 0;
}

#pragma region Formation

void AUnit::SetFormation(AFormation* NewFormation)
{
	if (Formation && Formation != NewFormation)
	{
		Formation->RemoveMember(this);
	}
	Formation = NewFormation;
}

#pragma endregion Formation

#pragma region Gameplay Tags interface

void AUnit::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.AddTag(UnitType);
	TagContainer.AppendTags(GameplayTagContainer);
}

#pragma endregion Gameplay Tags interface

#pragma region Selectable interface

void AUnit::Select_Implementation()
{
	Selected = true;

	CircleDecalComponent->SetIsSelected(true, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void AUnit::Unselect_Implementation()
{
	Selected = false;

	CircleDecalComponent->SetIsSelected(false, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void AUnit::Hover_Implementation()
{
	Hovered = true;

	CircleDecalComponent->SetIsHovered(true, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

void AUnit::Unhover_Implementation()
{
	Hovered = false;

	CircleDecalComponent->SetIsHovered(false, UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(this));
}

#pragma endregion Selectable interface
