// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InGame/Selectable/SelectableInterface.h"
#include "GameplayTagsModule.h"
#include "GameplayTags.h"
#include "Unit.generated.h"

class USelectionDecalComponent;
class UMaterialInstanceDynamic;
class UUnitOrderManagerComponent;
class AFormation;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEvent_Unit, AUnit*, Unit);

UCLASS()
class TICTACTOE_API AUnit : public ACharacter, public ISelectableInterface, public IGameplayTagAssetInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure)
		static TArray<AUnit*> GetUnitsOfType(TArray<AUnit*> UnitsToFilter, FGameplayTag Type);

public:
	// Sets default values for this character's properties
	AUnit();

	virtual bool ReplicateSubobjects(class UActorChannel *Channel, class FOutBunch *Bunch, FReplicationFlags *RepFlags) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void OnRep_Owner() override;

public:	
	virtual void PostInitializeComponents() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintPure)
		float GetUnitRadius();

#pragma region Order Management

protected:
	UPROPERTY(EditAnywhere)
		UUnitOrderManagerComponent* OrderManager;

public:
	UFUNCTION(BlueprintPure)
		UUnitOrderManagerComponent* GetOrderManager() { return OrderManager; }

#pragma region Order Management

#pragma region Formation

protected:
	UPROPERTY()
		AFormation* Formation;

public:
	UFUNCTION(BlueprintCallable)
		void SetFormation(AFormation* NewFormation);

	UFUNCTION(BlueprintPure)
		AFormation* GetFormation() const { return Formation; }

#pragma endregion Formation

#pragma region Selection circle
	
protected:
	UPROPERTY(EditAnywhere)
		USelectionDecalComponent* CircleDecalComponent;

#pragma endregion Selection circle

#pragma region Selectable Interface

private:
	bool Selected;
	bool Hovered;

public:
	virtual void Select_Implementation() override;

	virtual void Unselect_Implementation() override;

	virtual void Hover_Implementation() override;

	virtual void Unhover_Implementation() override;

#pragma endregion Selectable Interface

#pragma region GameplayTag Asset Interface

protected:

	UPROPERTY(EditAnywhere)
		FGameplayTag UnitType;

	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;

	UPROPERTY(EditAnywhere)
		FGameplayTagContainer GameplayTagContainer;

public:
	UFUNCTION(BlueprintPure)
		FGameplayTag GetUnitType() { return UnitType; }

#pragma endregion GameplayTag Asset Interface
};
