// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InGame/AI/OrderManager/OrderManagerComponent.h"
#include "UnitOrderManagerComponent.generated.h"

class AUnit;
/**
 * 
 */
UCLASS()
class TICTACTOE_API UUnitOrderManagerComponent : public UOrderManagerComponent
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

protected:
	AUnit* OwnerUnit;

public:
	virtual void AddOrderInternal_Implementation(FOrderStruct NewOrder) override;
	virtual bool CanAddOrder_Implementation(FOrderStruct NewOrder) override;
};
