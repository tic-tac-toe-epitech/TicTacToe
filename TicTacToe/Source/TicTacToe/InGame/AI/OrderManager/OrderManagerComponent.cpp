// Fill out your copyright notice in the Description page of Project Settings.


#include "OrderManagerComponent.h"
#include "Libraries/DiplomacyLibrary.h"
#include "TicTacToe.h"

// Sets default values for this component's properties
UOrderManagerComponent::UOrderManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	bReplicates = true;
	bAutoActivate = true;
	bAutoRegister = true;
	// ...
}


// Called when the game starts
void UOrderManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UOrderManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UOrderManagerComponent::AddOrder(FOrderStruct NewOrder)
{
	if (CanAddOrder(NewOrder))
	{
		AddOrderInternal(NewOrder);
		OnOrderGiven.Broadcast(this, NewOrder);
	}
}

bool UOrderManagerComponent::CanAddOrder_Implementation(FOrderStruct NewOrder)
{
	return true;
}


void UOrderManagerComponent::AddOrderInternal_Implementation(FOrderStruct NewOrder)
{
	if (!NewOrder.bIsShift)
	{
		ClearOrders();
	}
	OrderQueue.Add(NewOrder);
	UE_LOG(TicTacLogOrderManagement, Log,
		TEXT("UOrderManagerComponent::AddOrderInternal_Implementation: Order given to '%s'."),
		*GetNameSafe(this));
}

void UOrderManagerComponent::ClearOrders_Implementation()
{
	OrderQueue.Empty();
}