// Fill out your copyright notice in the Description page of Project Settings.


#include "UnitOrderManagerComponent.h"
#include "InGame/Unit/Unit.h"
#include "InGame/AI/InGameAIController.h"

void UUnitOrderManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerUnit = Cast<AUnit>(GetOwner());
}

void UUnitOrderManagerComponent::AddOrderInternal_Implementation(FOrderStruct NewOrder)
{
	Super::AddOrderInternal_Implementation(NewOrder);
}

bool UUnitOrderManagerComponent::CanAddOrder_Implementation(FOrderStruct NewOrder)
{
	return Super::CanAddOrder_Implementation(NewOrder) && OwnerUnit != nullptr;
}