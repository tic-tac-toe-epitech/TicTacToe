// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InGame/AI/OrderManager/OrderManagerInclude.h"
#include "OrderManagerComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEvent_OrderManagerComponent_Order, UOrderManagerComponent*, OrderManager, FOrderStruct, Order);

UCLASS(BlueprintType, Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UOrderManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOrderManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UPROPERTY()
		FOrderStruct CurrentOrder;

	UPROPERTY()
		TArray<FOrderStruct> OrderQueue;

	UFUNCTION(BlueprintNativeEvent)
		void ExecuteNextOrder();
	virtual void ExecuteNextOrder_Implementation() {}

	UFUNCTION(BlueprintNativeEvent)
		void AddOrderInternal(FOrderStruct NewOrder);
	virtual void AddOrderInternal_Implementation(FOrderStruct NewOrder);

	UFUNCTION(BlueprintNativeEvent)
		bool CanAddOrder(FOrderStruct NewOrder);
	virtual bool CanAddOrder_Implementation(FOrderStruct NewOrder);

public:
	UFUNCTION(BlueprintCallable)
		void AddOrder(FOrderStruct NewOrder);

	UFUNCTION(BlueprintNativeEvent)
		void ClearOrders();
	virtual void ClearOrders_Implementation();

	UPROPERTY(BlueprintAssignable)
		FEvent_OrderManagerComponent_Order OnOrderGiven;

	UPROPERTY(BlueprintAssignable)
		FEvent_OrderManagerComponent_Order OnOrderExecutionStarted;

	UPROPERTY(BlueprintAssignable)
		FEvent_OrderManagerComponent_Order OnOrderExecutionStopped;
};
