#pragma once

#include "CoreMinimal.h"
#include "OrderManagerInclude.generated.h"

UENUM(BlueprintType)
enum class EOrderType : uint8
{
	MOVE,
};

USTRUCT(BlueprintType)
struct FOrderStruct
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)
		EOrderType OrderType;

	UPROPERTY(BlueprintReadWrite)
		FVector TargetLocation;

	UPROPERTY(BlueprintReadWrite)
		bool bIsShift;
};