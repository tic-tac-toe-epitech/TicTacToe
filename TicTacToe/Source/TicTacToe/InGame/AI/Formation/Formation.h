// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Formation.generated.h"

class AUnit;
class UArrowComponent;

DECLARE_LOG_CATEGORY_EXTERN(TicTacLogUnitFormation, All, All);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEvent_Formation, AFormation*, Formation);

UCLASS()
class TICTACTOE_API AFormation : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFormation();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		UArrowComponent* DebugArrow;

	UPROPERTY()
		TArray<AUnit*> Members;

	UPROPERTY(Replicated)
		FVector Destination;

	UPROPERTY()
		bool IsDeparted;

	UPROPERTY()
		float Speed;

	UPROPERTY()
		TMap<AUnit*, FVector> MembersPositionMap;

	UPROPERTY(Replicated)
		TArray<FVector> Points;

	UPROPERTY()
		int32 MembersPerLine;

	UPROPERTY()
		int32 SpaceBetweenMembers;

	UPROPERTY()
		int32 SpaceBetweenLines;

	UFUNCTION(BlueprintCallable)
		void Clear();
		
private:
	void SetStartPosition();

	void SetSpeed();

	void SetMembersPosition();

	void CalculatePoints(TMap<FVector, FVector>& OutPoints);

	void HandleArrival();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
		const TArray<AUnit*>& GetMembers() { return Members; }

	UFUNCTION(BlueprintCallable)
		void SetMembers(const TArray<AUnit*>& NewMembers, bool UpdateFormationData = false);

	UFUNCTION(BlueprintCallable)
		void AddMember(AUnit* NewMember, bool UpdateFormationData = true);

	UFUNCTION(BlueprintCallable)
		void RemoveMember(AUnit* Member, bool UpdateFormationData = true);

	UFUNCTION(BlueprintPure)
		FVector GetAlignedMemberPosition(const AUnit* Member) const;

	UFUNCTION(BlueprintPure)
		FVector AlignRelativePositionWithFormation(const FVector& Position) const;

	UFUNCTION(BlueprintNativeEvent)
		void MoveToLocation(const FVector& NewDestination);
	void MoveToLocation_Implementation(const FVector& NewDestination);

	UFUNCTION(NetMulticast, Reliable)
		void SetTickableRPCM(bool Tickable);
	void SetTickableRPCM_Implementation(bool Tickable);

	UPROPERTY(BlueprintAssignable)
		FEvent_Formation OnFormationDeparted;

	UPROPERTY(BlueprintAssignable)
		FEvent_Formation OnFormationArrived;
};
