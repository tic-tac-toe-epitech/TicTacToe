// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "OrderManager/OrderManagerInclude.h"
#include "InGameAIController.generated.h"

class UOrderManagerComponent;
/**
 * 
 */
UCLASS()
class TICTACTOE_API AInGameAIController : public AAIController
{
	GENERATED_BODY()
	
protected:
	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION()
		void OnOrderReceived(UOrderManagerComponent* OrderManager, FOrderStruct NewOrder);
};
