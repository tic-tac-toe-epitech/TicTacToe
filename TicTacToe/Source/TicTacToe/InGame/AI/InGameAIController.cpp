// Fill out your copyright notice in the Description page of Project Settings.


#include "InGameAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "OrderManager/OrderManagerComponent.h"
#include "InGame/Unit/Unit.h"
#include "Formation/Formation.h"


void AInGameAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	UOrderManagerComponent* orderManager = InPawn->FindComponentByClass<UOrderManagerComponent>();

	if (orderManager)
	{
		orderManager->OnOrderGiven.AddDynamic(this, &AInGameAIController::OnOrderReceived);
	}
}

void AInGameAIController::OnOrderReceived(UOrderManagerComponent* OrderManager, FOrderStruct NewOrder)
{
	GetBlackboardComponent()->SetValueAsVector(TEXT("TargetLocation"), NewOrder.TargetLocation);
}