#pragma once

#include "CoreMinimal.h"
#include "TicTacToe/GameData/Configuration/Shared/Price.h"
#include "ActionCondition.generated.h"

//!
//! @struct FActionConditionStruct
//! @brief List all conditions to allow a action.
//!
USTRUCT(BlueprintType)
struct FActionConditionStruct
{
	GENERATED_USTRUCT_BODY()

	public:
		// Minimal HP to allow a action.
		UPROPERTY()
		int32				_Hp;
		// Resources minimal allow to a action.
		UPROPERTY()
		FPriceStruct		_Price;
		// Range minimal to allow a action.
		UPROPERTY()
		int32				_Range;
		// Scope needed to allow a action.
		UPROPERTY()
		int32				_Scope;
		// Alert if a action is buildable.
		UPROPERTY()
		bool				_BuildAllow;
		// Resources minimal allow to a upgrade action.
		UPROPERTY()
		FPriceStruct		_UpgradeCost;
		// All evolutions requires list for a action to be allow.
		UPROPERTY()
		TMap<FString, bool>	_EvolutionsRequire;

	public:
		//!
		//! @brief Tell if all conditions are valid in a actions context.
		//! @param actionCondition Current conditions of the game.
		//! @return State.
		//!
		bool	isValid(const FActionConditionStruct &actionCondition);

};