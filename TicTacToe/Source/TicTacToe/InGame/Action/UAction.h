// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "TicTacToe/InGame/Action/ActionCondition.h"
#include "UObject/NoExportTypes.h"
#include "UAction.generated.h"

/**
 * @class UUAction
 * @brief Defining what is a action.
 */
UCLASS(abstract)
class TICTACTOE_API UAction : public UObject
{
	GENERATED_BODY()

	public:
		// Gameplay tag defining the action name.
		UPROPERTY()
		FGameplayTag			_Tag;
		// Action description.
		UPROPERTY()
		FString					_Description;
		// Conditions allowing the action. Whithout thoses conditions, the action is not runnable.
		UPROPERTY()
		FActionConditionStruct	_Conditions;

	public:
		//!
		//! @brief Initialize the action tag name.
		//! @param actionTag Tag name. Must be a existing tag.
		//!
		inline void	setTag(const FGameplayTag &actionTag) { _Tag = FGameplayTag(actionTag); }
		//!
		//! @brief Initialize the action description.
		//! @param actionDescription Action description.
		//!
		inline void	setDescription(const FString &actionDescription) { _Description = actionDescription; }
		//!
		//! @brief Initialize the action conditions.
		//! @param actionConditions Action conditions.
		//!
		inline void	setConditions(const FActionConditionStruct &actionConditions) { _Conditions = actionConditions; }
		//!
		//! @brief Extract the tag name.
		//! @return Action tag name.
		//!
		inline const FGameplayTag			&getTag() const { return (_Tag); }
		//!
		//! @brief Extract the action description.
		//! @return Action description.
		//!
		inline const FString				&getDescription() const { return (_Description);  }
		//!
		//! @brief Extract the action conditions.
		//! @return Action conditions.
		//!
		inline const FActionConditionStruct	&getCondition() const { return (_Conditions); }

	public:
		//!
		//! @brief Compute if the action is runnable. This method must be develop for each action.
		//! @return Status, runnable or not.
		//!
		virtual bool	isRunnable() const PURE_VIRTUAL(UAction::isRunnable, return (false););
		//!
		//! @brief Run the action, this function contain the action logic. This method must be develop for each action.
		//! @return Status, runnable or not.
		//!
		virtual void	run() PURE_VIRTUAL(UAction::isRunnable, ;);
};
