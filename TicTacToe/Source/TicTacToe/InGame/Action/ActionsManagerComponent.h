// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TicTacToe/InGame/Action/UAction.h"
#include "Components/ActorComponent.h"
#include "ActionsManagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TICTACTOE_API UActionsManagerComponent : public UActorComponent
{
	GENERATED_BODY()

	public:
		// List of all available action.
		TArray<UAction*>	_Actions;
		// Action stack. All actions in this stack should be run in order according all conditions.
		TArray<UAction*>	_ActionsStack;
		// Current action working on.
		UAction*			_CurrentAction;

	public:	
		// Sets default values for this component's properties
		UActionsManagerComponent();

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	public:
		//!
		//! @brief Initialize the action manager.
		//! @brief Extract all data from global projet setting.
		//! @warning Currently not available.
		//!
//		void   init();
		//!
		//! @brief Initialize the action manager.
		//! @param actions List of actions available to execute. Any other actions will be autorized.
		//!
		void   init(const TArray<UAction*> &actions); // TO FINISH
		//!
		//! @brief Add a action to the action manager.
		//! @param action Action to add.
		//!
		void   add(const UAction &action); // TODO
		//!
		//! @brief Remove a action to the action manager.
		//! @param actionTag Action tag use to find the action to remove.
		//!
		void   remove(const FString &actionTag); // TODO

};
