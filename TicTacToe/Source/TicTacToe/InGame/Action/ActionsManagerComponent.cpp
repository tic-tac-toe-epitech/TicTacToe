// Fill out your copyright notice in the Description page of Project Settings.


#include "TicTacToe/InGame/Action/ActionsManagerComponent.h"

// Sets default values for this component's properties
UActionsManagerComponent::UActionsManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UActionsManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UActionsManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void   UActionsManagerComponent::init(const TArray<UAction*> &actions)
{
	UE_LOG(ActionsManager, Log, TEXT("Actions from the ActionsManager is initialize."));
//	_Actions = actions; 
// OPERATOR '=' A FAIRE !!!!
	for (UAction* action : _Actions)
	{
		if (action->getTag().ToString().IsEmpty())
		{
			UE_LOG(ActionsManager, Log, TEXT("Invalid action has detected."));
//			_Actions.Remove(action);
			// OPERATOR '==' A FAIRE !!!
		}
		else
			UE_LOG(ActionsManager, Log, TEXT("This action has been added to the available action list : %s."), *(action->getTag().GetTagName().ToString()));
	}
}

void   UActionsManagerComponent::add(const UAction &action)
{

}

void   UActionsManagerComponent::remove(const FString &actionTag)
{

}
