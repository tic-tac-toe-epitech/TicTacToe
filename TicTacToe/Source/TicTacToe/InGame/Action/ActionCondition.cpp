#include "TicTacToe/InGame/Action/ActionCondition.h"
#include "TicTacToe/TicTacToe.h"

bool	FActionConditionStruct::isValid(const FActionConditionStruct &actionCondition)
{ 
	bool	isValid = true;

	if (actionCondition._Hp < _Hp)
	{
		isValid = false;
		UE_LOG(Action, Error, TEXT("Not enough HP. This action require %d hp and you only have %d hp."), _Hp, actionCondition._Hp);
	}
	if (actionCondition._Price < _Price)
	{
		isValid = false;
		UE_LOG(Action, Error, TEXT("Not enough resources."));
		UE_LOG(Action, Log, TEXT("Current resources : "));
		_Price.logIt();
		UE_LOG(Action, Log, TEXT("Needed resources : "));
		actionCondition._Price.logIt();
	}
	if (actionCondition._Range < _Range)
	{
		isValid = false;
		UE_LOG(Action, Error, TEXT("Not enough range. This action require %d range and you only have %d range."), _Range, actionCondition._Range);
	}
	if (actionCondition._Scope < _Scope)
	{
		isValid = false;
		UE_LOG(Action, Error, TEXT("Not enough scope. This action require %d scope and you only have %d scope."), _Scope, actionCondition._Scope);
	}
	if (_BuildAllow && !actionCondition._BuildAllow)
	{
		isValid = false;
		UE_LOG(Action, Error, TEXT("This action must allow builds."));
	}
	if (actionCondition._UpgradeCost < _UpgradeCost)
	{
		isValid = false;
		UE_LOG(Action, Error, TEXT("This action must require upgrade."));
		UE_LOG(Action, Error, TEXT("Not enough resources."));
		UE_LOG(Action, Log, TEXT("Current resources : "));
		_Price.logIt();
		UE_LOG(Action, Log, TEXT("Needed resources : "));
		actionCondition._Price.logIt();
	}
	for (auto evolve : _EvolutionsRequire)
	{
		if (!actionCondition._EvolutionsRequire.Find(evolve.Key) && !evolve.Value)
		{
			isValid = false;
			UE_LOG(Action, Error, TEXT("The next element (%s) need to be evoluted and are currently not."), *(evolve.Key));
		}
	}
	return (isValid);
}