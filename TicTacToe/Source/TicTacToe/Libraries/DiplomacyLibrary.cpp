// Fill out your copyright notice in the Description page of Project Settings.


#include "DiplomacyLibrary.h"
#include "Engine/Player.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/NetConnection.h"
#include "TicTacToe.h"

EDiplomacy UDiplomacyLibrary::GetDiplomacyWithPlayerController(AActor* Actor, APlayerController* PlayerController)
{
	if (PlayerController)
	{
		return GetDiplomacyWithPlayerState(Actor, PlayerController->GetPlayerState<APlayerState>());
	}
	return EDiplomacy::NEUTRAL;
}

EDiplomacy UDiplomacyLibrary::GetDiplomacyWithPlayerState(AActor* Actor, APlayerState* PlayerState)
{
	if (!Actor)
	{
		UE_LOG(TicTacLogDiplomacy, Warning,
			TEXT("UDiplomacyLibrary::GetDiplomacyWithPlayerState: Given actor is invalid."));
		return EDiplomacy::NEUTRAL;
	}
	APlayerState* actorOwningPlayerState = Cast<APlayerState>(Actor->GetOwner());

	// if actor has no owning player state, we assume it is neutral with the given player state, whoever that is.
	if (actorOwningPlayerState)
	{
		// if both actor player state and given player state exist and are the same, then this actor is owned by the given player.
		if (actorOwningPlayerState == PlayerState)
		{
			return EDiplomacy::OWNED;
		}
		// if both given player state and actor owning player state exist but they are different, then actor is an enemy of given player state.
		else if (PlayerState)
		{
			return EDiplomacy::ENEMY;
		}
	}
	return EDiplomacy::NEUTRAL;
}

EDiplomacy UDiplomacyLibrary::GetDiplomacyWithPlayer(AActor* Actor, UPlayer* Player)
{
	if (Player && Actor)
	{
		return GetDiplomacyWithPlayerController(Actor, Player->GetPlayerController(Actor->GetWorld()));
	}
	return EDiplomacy::NEUTRAL;
}

EDiplomacy UDiplomacyLibrary::GetDiplomacyWithActor(AActor* Actor, AActor* OtherActor)
{
	if (OtherActor)
	{
		return GetDiplomacyWithPlayerController(Actor, Cast<APlayerController>(OtherActor->GetOwner()));
	}
	return EDiplomacy::NEUTRAL;
}

EDiplomacy UDiplomacyLibrary::GetDiplomacyWithLocalPlayer(AActor* Actor)
{
	// Dedicated server is always neutral with all actors.
	if (IsRunningDedicatedServer())
	{
		return EDiplomacy::NEUTRAL;
	}
	return GetDiplomacyWithPlayerController(Actor, UGameplayStatics::GetPlayerController(Actor, 0));
}
