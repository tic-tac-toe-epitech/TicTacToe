// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Misc/AutomationTest.h"
#include "Modules/ModuleManager.h"

DECLARE_LOG_CATEGORY_EXTERN(TicTacLogMapGeneration, All, All);
DECLARE_LOG_CATEGORY_EXTERN(TicTacLogPlayerCivilization, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(TicTacLogSelection, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Configuration, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(Action, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(ActionsManager, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(TicTacLogOrderManagement, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(TicTacLogDiplomacy, Log, All);

class FTicTacToeGameModuleImpl : public FDefaultGameModuleImpl {

	void ShutdownModule() override {
		TArray< FAutomationTestInfo > AllTest;
		FAutomationTestFramework::Get().GetValidTestNames(AllTest);

		for (FAutomationTestInfo TestInfo : AllTest) {
			FAutomationTestFramework::Get().UnregisterAutomationTest(TestInfo.GetTestName());
		}
		FAutomationTestFramework::Get().ResetTests();
	}
};
