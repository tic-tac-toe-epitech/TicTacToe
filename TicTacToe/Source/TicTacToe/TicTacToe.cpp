// Fill out your copyright notice in the Description page of Project Settings.

#include "TicTacToe.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FTicTacToeGameModuleImpl, TicTacToe, "TicTacToe" );

DEFINE_LOG_CATEGORY(TicTacLogMapGeneration);
DEFINE_LOG_CATEGORY(TicTacLogPlayerCivilization);
DEFINE_LOG_CATEGORY(TicTacLogSelection);
DEFINE_LOG_CATEGORY(Configuration);
DEFINE_LOG_CATEGORY(Action);
DEFINE_LOG_CATEGORY(ActionsManager);
DEFINE_LOG_CATEGORY(TicTacLogOrderManagement);
DEFINE_LOG_CATEGORY(TicTacLogDiplomacy);
