#include "CoreMinimal.h"
#include "UnitTest.h"
#include "TicTacToe/Tests/Unit/CustomUnitTest.h"
#include "TicTacToe/GameData/Configuration/ConfigurationLoader.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(GetUnitsConfig, FCustomUnitTest, "UnitTests.UConfigurationLoaderTest.GetUnitsConfig", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool GetUnitsConfig::RunTest(const FString& Parameters)
{
	UConfigurationLoader	*configurationLoader = NewObject<UConfigurationLoader>();
	FUnitsStruct			units;

	MessageInfo(TEXT("Checking fews default values for units configuration."));
	units = configurationLoader->GetUnitsConfig();
	return (units._Id == "Units" && units._Artillery._Damage._Value == 30 && units._Cyborg._Hp == 200 && units._Scout._Name == "Scout");
}