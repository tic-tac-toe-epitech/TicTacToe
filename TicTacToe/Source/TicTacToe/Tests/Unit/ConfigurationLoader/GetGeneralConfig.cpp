#include "CoreMinimal.h"
#include "UnitTest.h"
#include "TicTacToe/Tests/Unit/CustomUnitTest.h"
#include "TicTacToe/GameData/Configuration/ConfigurationLoader.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(GetGeneralConfig, FCustomUnitTest, "UnitTests.UConfigurationLoaderTest.GetGeneralConfig", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool GetGeneralConfig::RunTest(const FString& Parameters)
{
	UConfigurationLoader	*configurationLoader = NewObject<UConfigurationLoader>();
	FGeneralStruct			general;

	MessageInfo(TEXT("Checking all default values for general configuration."));
	general = configurationLoader->GetGeneralConfig();
	return (general._Network._Url == "localhost" && general._Network._Port == 4242);
}