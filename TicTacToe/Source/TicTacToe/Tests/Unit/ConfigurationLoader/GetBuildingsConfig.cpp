#include "CoreMinimal.h"
#include "UnitTest.h"
#include "TicTacToe/Tests/Unit/CustomUnitTest.h"
#include "TicTacToe/GameData/Configuration/ConfigurationLoader.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(GetBuildingsConfigDefaultValue, FCustomUnitTest, "UnitTests.UConfigurationLoaderTest.GetBuildingsConfig", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool GetBuildingsConfigDefaultValue::RunTest(const FString& Parameters)
{
	UConfigurationLoader	*configurationLoader = NewObject<UConfigurationLoader>();
	FBuildingsStruct		buildings;

	MessageInfo(TEXT("Checking fews default values for buildings configuration."));
	buildings = configurationLoader->GetBuildingsConfig();
	return (buildings._Id == "Building" && buildings._CityHall._Hp == 2500 && buildings._Hospital._Name == "Hospital" &&
			buildings._Forge._BuildAllow == 1);
}