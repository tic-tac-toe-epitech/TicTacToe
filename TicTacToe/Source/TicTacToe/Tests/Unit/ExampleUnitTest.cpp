// Fill out your copyright notice in the Description page of Project Settings.

#include "CoreMinimal.h"
#include "UnitTest.h"
#include "CustomUnitTest.h"

IMPLEMENT_CUSTOM_SIMPLE_AUTOMATION_TEST(UExampleUnitTest, FCustomUnitTest, "UnitTests.UExampleUnitTest", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

bool UExampleUnitTest::RunTest(const FString& Parameters)
{
	this->MessageInfo(TEXT("Start Sleep 5s"));
	FPlatformProcess::Sleep(5);
	this->MessageInfo(TEXT("Finish Sleep"));
	return true;
}