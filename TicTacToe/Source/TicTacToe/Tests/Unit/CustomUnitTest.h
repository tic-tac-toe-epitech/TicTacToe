#pragma once

#include "AutomationTest.h"
#include "CoreMinimal.h"
#include "UnitTest.h"
#include "Logging/MessageLog.h"

class FCustomUnitTest : public FAutomationTestBase
{
public:
	FCustomUnitTest(const FString& InName, const bool bInComplexTask)
		: FAutomationTestBase(InName, bInComplexTask) {}

	void MessageInfo(FString Message) {
		this->AddInfo(Message);
	}

	void MessageWarning(FString Message) {
		this->AddWarning(Message);
	}

	void MessageError(FString Message) {
		this->AddError(Message);
	}

};