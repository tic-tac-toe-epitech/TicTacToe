// Fill out your copyright notice in the Description page of Project Settings.


#include "TimeOutFunctionalTest.h"
#include "Logging/MessageLog.h"
#include "Kismet/KismetSystemLibrary.h"

void ATimeOutFunctionalTest::PrepareTest() {
	this->MessageInfo(TEXT("PrepareTest."));
	this->SetTimeLimit(3, EFunctionalTestResult::Error);
	this->MessageInfo(TEXT("SetTimeLimit 3."));
	this->IsReady();
	Super::PrepareTest();
}

void ATimeOutFunctionalTest::StartTest() {
	this->MessageInfo(TEXT("StartTest."));
	Super::StartTest();
}

bool ATimeOutFunctionalTest::RunTest(const TArray< FString >& Params) {
	this->MessageInfo(TEXT("RunTest."));
	return Super::RunTest(Params);
}

void ATimeOutFunctionalTest::Tick(float delta) {
	Super::Tick(delta);
}	//FMessageLog("Automation Testing Log").Warning(FText::FromString(TEXT("Tick.")));


void ATimeOutFunctionalTest::OnTimeout() {
	this->MessageInfo(TEXT("OnTimeout."));
	Super::OnTimeout();
}

void ATimeOutFunctionalTest::FinishTest(EFunctionalTestResult TestResult, const FString& Message) {
	this->MessageInfo(TEXT("Finish Test."));
	Super::FinishTest(TestResult, Message);
	UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, true);
}