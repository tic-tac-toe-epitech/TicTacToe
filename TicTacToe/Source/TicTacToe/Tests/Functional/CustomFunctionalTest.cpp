#include "CustomFunctionalTest.h"

void ACustomFunctionalTest::MessageInfo(FString Message, bool onScreen) {
	if (GEngine && onScreen) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, *Message);
	}
	this->logger.Info(FText::FromString(Message));
}

void ACustomFunctionalTest::MessageWarning(FString Message, bool onScreen) {
	if (GEngine && onScreen) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, *Message);
	}
	this->logger.Warning(FText::FromString(Message));
}

void ACustomFunctionalTest::MessageError(FString Message, bool onScreen) {
	if (GEngine && onScreen) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, *Message);
	}
	this->logger.Error(FText::FromString(Message));
}
