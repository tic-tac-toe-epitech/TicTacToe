// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CustomFunctionalTest.h"
#include "HundredTickFunctionalTest.generated.h"

/**
 * 
 */
UCLASS()
class TICTACTOE_API AHundredTickFunctionalTest : public ACustomFunctionalTest
{
	GENERATED_BODY()

private:
	int nbTick = 0;

public:
		
	FString Description = TEXT("Functional Test Example 2");
	FString Author = TEXT("Arthur Grosso");

	virtual void StartTest();
	virtual void PrepareTest();
	virtual void FinishTest(EFunctionalTestResult TestResult, const FString& Message);

	virtual bool RunTest(const TArray< FString >& Params);
	virtual void Tick(float delta);
};
