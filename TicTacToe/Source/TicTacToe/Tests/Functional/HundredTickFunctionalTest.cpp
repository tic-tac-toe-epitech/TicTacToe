// Fill out your copyright notice in the Description page of Project Settings.


#include "HundredTickFunctionalTest.h"
#include "Logging/MessageLog.h"
#include "Kismet/KismetSystemLibrary.h"

void AHundredTickFunctionalTest::PrepareTest() {
	this->nbTick = 0;
	this->MessageInfo(TEXT("PrepareTest."));
	this->IsReady();
	Super::PrepareTest();
}

void AHundredTickFunctionalTest::StartTest() {
	this->MessageInfo(TEXT("StartTest."));
	Super::StartTest();
}

bool AHundredTickFunctionalTest::RunTest(const TArray< FString >& Params) {
	this->MessageInfo(TEXT("RunTest."));
	return Super::RunTest(Params);
}

void AHundredTickFunctionalTest::Tick(float delta) {
	Super::Tick(delta);
	if (this->nbTick >= 100) {
		this->FinishTest(EFunctionalTestResult::Succeeded, TEXT("100 Frames passed."));
	}
	this->nbTick += 1;
}


void AHundredTickFunctionalTest::FinishTest(EFunctionalTestResult TestResult, const FString& Message) {
	UE_LOG(LogTemp, Warning, TEXT("FinishTest."));
	this->MessageInfo(TEXT("FinishTest."));
	Super::FinishTest(TestResult, Message);
	UKismetSystemLibrary::QuitGame(this, nullptr, EQuitPreference::Quit, true);
}