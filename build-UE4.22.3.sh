!#/bin/bash

set -a
. ./Config.txt
set +a

echo "--- INSTALL DEPENDENCIES ---"
#https://github.com/EpicGames/UnrealEngine/blob/4.22.3-release/Engine/Build/BatchFiles/Linux/README.md
echo "INSTALL MONO"
sudo apt-get install mono
echo "INSTALL CLANG"
sudo apt-get install clang

echo "--- SETUP UNREALENGINE ---"
cd $UE4_ROOT
./Setup.sh
./GenerateProjectFiles.sh

echo "--- BUILD UNREALENGINE ---"
make UE4Editor UE4Game UnrealPak CrashReportClient ShaderCompileWorker UnrealLightmass

#echo "--- GENERATE PROJECT FILES ---"
#$UE4_ROOT/GenerateProjectFiles.sh -project="$GAME_UPROJECT" -game

